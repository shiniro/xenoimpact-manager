/*
* Main
* Dependensies: Models - Pages 
* (Homepage, Authentification - FindUser)
* Configuration : theme (font, colors) - route
* AutoAuthentification if there is data in cookie
*/

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'widgets/helpers/global_translation.dart';
// import 'package:flutter/rendering.dart'; //for debug 

import 'models/request.dart';

import 'scoped-models/main.dart';

//homepage
import 'pages/home.dart';
import 'pages/leaves/request_leave.dart';

//authentification
import 'pages/authentification/authentification.dart';
import 'pages/authentification/forgot_password.dart';

//filter
import 'pages/leaves/requests_filter.dart';

//modify request
import 'pages/leaves/request_modify.dart';
import 'pages/admin/manage_request_status.dart';

//admin
import 'pages/admin/manage_requests.dart';
import 'pages/admin/manage_users.dart';
import 'pages/admin/manage_user.dart';
import 'pages/admin/company_settings.dart';

//settings
import 'pages/settings/modify_profil.dart';
import 'pages/settings/change_password.dart';

void main() async {
  // Initializes the translation module
  await allTranslations.init();
  
  // debugPaintSizeEnabled = true; //for debug 
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  bool _isAuthentificated = false;

  /********* Init State **********/
  @override
  void initState() {
    // _model.autoAuthenticate();
    _model.userSubject.listen((bool isAuthentificated) {
      setState(() {
        _isAuthentificated = isAuthentificated;
      });
    });

    // Initializes a callback should something need 
    // to be done when the language is changed
    allTranslations.onLocaleChangedCallback = _onLocaleChanged;

    super.initState();
  }



  ///
  /// If there is anything special to do when the user changes the language
  ///
  _onLocaleChanged() async {
      // do anything you need to do if the language changes
      print('Language has been changed to: ${allTranslations.currentLanguage}');
  }




  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        theme: _buildAppTheme(),
        localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
        ],
        // Tells the system which are the supported languages
        supportedLocales: allTranslations.supportedLocales(),
        routes: {
          '/': (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : HomePage(_model),
          '/admin/manage-leaves':  (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : ManageRequestsPage(_model),
          '/admin/users':  (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : UsersPage(_model),
          '/admin/company':  (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : CompanySettings(_model),
          '/settings':  (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : ModifyProfilPage(_model),
          '/settings/password':  (BuildContext context) => !_isAuthentificated ? AuthentificationPage(_model) : ChangePasswordPage(_model),
        },
        onGenerateRoute: (RouteSettings settings) {
          final List<String> pathElements = settings.name.split('/');

          if (pathElements[0] != '') return null;

          /********  only if not authentificarted *******/
          if (!_isAuthentificated) { 
            // register page
            if (pathElements[1] == 'register') { 
                return MaterialPageRoute<bool>(
                  builder: (BuildContext context) => 
                  AuthentificationPage(_model),
                );
              // forgot password page
            } else if (pathElements[1] == 'forgot-password') { 
                return MaterialPageRoute<bool>(
                  builder: (BuildContext context) => 
                  ForgotPasswordPage(_model),
                );
            }
            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => AuthentificationPage(_model)
            );
          } /********  only if authentificarted *******/
          else {
            // request info / modify request
            if (pathElements[1] == 'request') {
              // modify status
              if(pathElements[2] == 'status') {
                final String requestId = pathElements[3];
                _model.selectRequest(requestId);
                return MaterialPageRoute<dynamic>(
                  builder: (BuildContext context) => 
                  ManageRequestStatus(_model),
                );
                // modify request
              } else {
                final String requestId = pathElements[2];
                if(requestId != '') {
                  final Request request = _model.allRequests.firstWhere((Request request) {
                    return  request.id == requestId;
                  });
                  _model.selectRequest(requestId);
                  return MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) => 
                    ModifyRequestPage(_model, request),
                  );
                } else {
                  return MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) => 
                    RequestLeavePage(_model),
                  );
                }
              }
            }


            //user page
            if (pathElements[1] == 'admin' && pathElements[2] == 'users') {
              final String userId = pathElements[3];
              _model.selectUser(userId);
              return MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => 
                ManageUserPage(_model),
              );
            }
            
            // filter page
            if (pathElements[1] == 'filters') {
                final String page = pathElements[2];
                return MaterialPageRoute<bool>(
                  builder: (BuildContext context) => 
                  RequestsFilterPage(_model, page),
                );
            }
          }

          return null;
        },
        onUnknownRoute: (RouteSettings settings) {
          print(settings);
          return MaterialPageRoute(
            builder: (BuildContext context) => HomePage(_model)
          );
        },
      ),
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * Build Theme
  */
  ThemeData _buildAppTheme() {
    return ThemeData(
      accentColor: Color(0xff07adac),
      primaryColor: Color(0xff07adac),
      buttonColor: Color(0xff07adac),
      dividerColor: Color(0xffcccccc),
      appBarTheme: AppBarTheme(color: Color(0xff07adac)),
      fontFamily: 'NoteSans',
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      textTheme: TextTheme(
        headline: TextStyle(fontWeight: FontWeight.w500),
        title: TextStyle(fontWeight: FontWeight.w500),
        body1: TextStyle(fontSize: 13.0, color: Color(0xff8290ad)),
        body2: TextStyle(fontSize: 14.0, color: Color(0xff333333)),
        button: TextStyle(fontSize: 15.0, color: Colors.white),
      ),
    );
  }
}