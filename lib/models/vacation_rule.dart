/*
* Leave Model
* 
*/

import 'package:flutter/material.dart';

class VacationRule {
  final String id;
  final String name;
  final bool autoActionOnChangeYear;
  final bool resetOnChangeYear;
  final double nbDaysOffPerYear;
  final double nbSummerVacationPerYear;
  final String resetDate;
  final bool addEachMonth;
  final double nbDayAddEachMonth;
  final String summerVacationAddDate;

  VacationRule({
    @required this.id,
    @required this.name,
    @required this.autoActionOnChangeYear,
    @required this.resetOnChangeYear,
    @required this.nbDaysOffPerYear,
    @required this.nbSummerVacationPerYear,
    this.resetDate,
    this.addEachMonth,
    this.nbDayAddEachMonth,
    this.summerVacationAddDate
  });
}