/*
* Leave Model
* 
*/

import 'package:flutter/material.dart';

class Company {
  final String id;
  final String name;
  final String email;
  final String emailBase;
  final String phone;
  final String language;
  final String address;
  final String country;
  final String vacationRule;
  final String defaultRule;

  Company({
    @required this.id,
    @required this.name,
    @required this.email,
    @required this.emailBase,
    @required this.phone,
    @required this.language,
    @required this.address,
    @required this.country,
    this.vacationRule,
    @required this.defaultRule
  });
}