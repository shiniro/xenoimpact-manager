/*
* User Model
* class User: the user that login the account - get information after login
*/

import 'package:flutter/material.dart';

import './company.dart';
import './vacation_rule.dart';

class User {
  final String id;
  final String email;
  final String name;
  final String password;
  final String pictureUrl;
  final Company company;
  final VacationRule vacationRule;
  final String os;
  final String pushId;
  final String token;
  final bool admin;
  final bool superAdmin;
  final double daysOff;
  final double summerVacation;
  final double summerVacationTaken;
  final double daysOffTaken;
  final double daysOffNoPaidTaken;
  final double medicalLeaveTaken;
  final double nbDaysOffPerYear;
  final double nbSummerVacationPerYear;
  final String language;
  final DateTime lastSignUp;
  final int expiryTime;
  final bool autoActionOnChangeYear;
  final bool resetOnChangeYear;

  User({
    @required this.id,
    @required this.email,
    this.name,
    this.password,
    this.pictureUrl,
    this.company,
    this.vacationRule,
    @required this.os,
    @required this.pushId,
    @required this.token,
    @required this.admin,
    @required this.superAdmin,
    @required this.daysOff,
    @required this.summerVacation,
    @required this.summerVacationTaken,
    @required this.daysOffNoPaidTaken,
    @required this.daysOffTaken,
    @required this.medicalLeaveTaken,
    this.nbDaysOffPerYear,
    this.nbSummerVacationPerYear,
    this.language,
    this.lastSignUp,
    this.expiryTime,
    @required this.autoActionOnChangeYear,
    @required this.resetOnChangeYear
  });
}