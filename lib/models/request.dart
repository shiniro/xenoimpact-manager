/*
* Leave Model
* 
*/

import 'package:flutter/material.dart';

class Request {
  final String id;
  final double nbDays;
  final DateTime startDate;
  final DateTime endDate;
  final String type;
  final String status;
  final String fileUrl;
  final String filePath;
  final String user;
  final String userName;
  final String comment;

  Request({
    @required this.id,
    @required this.nbDays,
    @required this.startDate,
    @required this.endDate,
    @required this.type,
    @required this.status,
    this.fileUrl,
    this.filePath,
    @required this.user,
    @required this.userName,
    this.comment
  });
}