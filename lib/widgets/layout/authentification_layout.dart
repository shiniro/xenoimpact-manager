/*
* Global Layout
* Dependensies: Side Menu - App Header
* Build the global layout of the app
* Parameters :
* - title, showMenu, SingleChildScroll, horizontalPadding, verticalPadding, actions, body, bottomNavigationBar
*/

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';
import './global_layout.dart';

class AuthentificationLayout extends StatelessWidget {
  final Widget child;

  AuthentificationLayout({ this.child });

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      showAppHeader: false,
      body: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0.0,
              child: Image.asset(
                'assets/img/background_1.png',
                fit: BoxFit.fill,
                color: Colors.black.withOpacity(0.8),
                colorBlendMode: BlendMode.dstATop
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
              child: child
            ),
            ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
              return model.isLoading ? Positioned.fill(
                child: Opacity(
                  opacity: 0.5,
                  child: Container(
                    color: Colors.black,
                    child: Center(child: CircularProgressIndicator())
                  )
                ),
              ) : Container();
            })
          ]
        )
      )
    );
  }
}