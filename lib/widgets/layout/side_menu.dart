/*
* Side Menu
* Dependensies: Models
* Build the App Side Menu
*/
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';

import '../../widgets/ui_elements/user_image.dart';

class SideMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SideMenuState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class SideMenuState extends State<SideMenu> {
  final List<Map<String, String>> _menuLinks  = [
    {
      'title': allTranslations.text("side_menu.leaves_status"),
      'link': '/'
    },
    {
      'title': allTranslations.text("side_menu.manage_leaves"),
      'link': '/admin/manage-leaves'
    },
    {
      'title': allTranslations.text("side_menu.manage_users"),
      'link': '/admin/users'
    },
    {
      'title': allTranslations.text("settings.modify_profil"),
      'link': '/settings'
    },
    {
      'title': allTranslations.text("side_menu.company_settings"),
      'link': '/admin/company'
    },
    {
      'title': allTranslations.text("side_menu.logout"),
      'link': 'logout'
    }
  ];

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
        return ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            SizedBox(height: 50.0),
            model.user.pictureUrl != null
              ? _buildUserImage(model.user.pictureUrl)
              : Container(),
            model.user.pictureUrl != null 
              ? SizedBox(height: 20.0) 
              : Container(),
            _buildSideMenuTitle('${allTranslations.text("side_menu.hello")} ${model.user.name}'),
            SizedBox(height: 50.0),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: _menuLinks.length,
              itemBuilder: (context, index) {
                return Column(
                  children: <Widget>[
                    _menuLinks[index]['link'].contains('admin') && model.user.admin || !_menuLinks[index]['link'].contains('admin')
                      ? _buildMenuListTile(context, _menuLinks[index]['title'], _menuLinks[index]['link'], model.logout)
                      : Container(),
                    _menuLinks[index]['link'].contains('admin') && model.user.admin || !_menuLinks[index]['link'].contains('admin')
                      ? Divider(
                      height: 1.0,
                    ): Container(),
                  ],
                );
              },
            )
          ]
        );
      })
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * Build Side Menu Title
  */
  Widget _buildSideMenuTitle(String title) {
    return Center(
      child: Text(
        title,
        style: TextStyle(
          fontSize: 23.0,
          color: Theme.of(context).accentColor,
        )
      )
    );
  }

  /*
  * Build User Image
  */
  Widget _buildUserImage(String imagePath) {
    return Center (
      child: imagePath  != null 
        ? UserImage(imagePath: imagePath)
        : UserImage(imagePath: 'assets/img/user-no-picture.jpg', internalImage: true),
    );
  }


  /*
  * Build Menu List Title
  */
  Widget _buildMenuListTile(BuildContext context, String title, String link, Function logout) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(
          color: Colors.black,
          fontSize: 15.0,
        )
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 24.7, vertical: 0),
      onTap: () {
        if(link != 'logout') {
          bool isNewRouteSameAsCurrent = false;

          // Check if same route
          Navigator.popUntil(context, (route) {
          if (route.settings.name == link) {
            isNewRouteSameAsCurrent = true;
          }
            return true;
          });

          if (!isNewRouteSameAsCurrent) {
            Navigator.pushReplacementNamed(context, link);
          } else {
            Navigator.pop(context);
          }
        } else {
          logout();
        }
      }
    );
    // Divider();
  }
}