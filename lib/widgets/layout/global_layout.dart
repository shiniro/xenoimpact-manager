/*
* Global Layout
* Dependensies: Side Menu - App Header
* Build the global layout of the app
* Parameters :
* - title, showMenu, SingleChildScroll, horizontalPadding, verticalPadding, actions, body, bottomNavigationBar
*/

import 'package:flutter/material.dart';

import './side_menu.dart';
import './app_header.dart';

class GlobalLayout extends StatelessWidget {
  final String title;
  final List<Widget> actions;
  final bool showAppHeader;
  final IconButton appBarLeading;
  final int tabBarLength;
  final Widget bottom;
  final bool showMenu;
  final bool singleChildScroll;
  final double horizontalPadding;
  final double verticalPadding;
  final Widget body;
  final Widget bottomNavigationBar;
  final Widget floatingActionButton;

  GlobalLayout({
    this.title,
    this.showAppHeader = true,
    this.showMenu = true,
    this.appBarLeading,
    this.tabBarLength = 1,
    this.bottom,
    this.singleChildScroll = false,
    this.horizontalPadding = 0.0,
    this.verticalPadding = 0.0,
    this.actions,
    this.body,
    this.bottomNavigationBar,
    this.floatingActionButton
  });

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    return Scaffold(
      drawer: showMenu ? SideMenu() : null,
      appBar: showAppHeader ? AppHeader(title: title, actions: actions, leading: appBarLeading) : null,
      body: singleChildScroll
      ? Container(
        padding: EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: verticalPadding),
        color: Color(0xffedf1f2),
        child: SingleChildScrollView (
          child: Container(
            width: targetWidth,
            child: body,
          )
        )
      )
      : Container(
        padding: EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: verticalPadding),
        child: body,
      ),
      floatingActionButton: floatingActionButton,
      bottomNavigationBar: bottomNavigationBar
    );
  }
}