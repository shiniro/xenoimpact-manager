/*
* App Header
* Build the App header
* Parameters :
* - required: Title
* - optional: actions (right side of the title)
*/

import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final List<Widget> actions;
  final IconButton leading;

  AppHeader({
    this.title,
    this.actions,
    this.leading
  });

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: true,
      leading: leading,
      elevation: 0.0,
      title: Text(
        title,
        style: TextStyle(
          fontSize: 15.0,
          fontFamily: 'NoteSans',
          color: Colors.white
        ),
      ),
      backgroundColor: Theme.of(context).appBarTheme.color,
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      actions: actions,
    );
  }
}