import 'package:flutter/material.dart';

class StandardButton extends StatelessWidget {
  final String text;
  final Color color;
  final Color textColor;
  final Function onPressed;

  StandardButton({
    @required this.text,
    this.color,
    this.textColor,
    @required this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 175.0,
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 14),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          color: Theme.of(context).primaryColor,
          textColor: textColor != null ? textColor : Colors.white,
          child: Text(
            text.toUpperCase(),
            style: TextStyle(
              fontSize: 14.0,
              color: color != null ? color : Colors.white
            )
          ),
          onPressed: onPressed,
        )
      ),
    );
  }
}