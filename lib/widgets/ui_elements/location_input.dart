import 'package:flutter/material.dart';
import 'package:location/location.dart';

import '../helpers/location_helper.dart';

class LocationInput extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LocationInputState();
  }
}

class LocationInputState extends State<LocationInput> {

  String _previewImageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: <Widget>[
        Container(
          height: 170,
          width: double.infinity,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              color: Color(0xffd2d5e4)
            ) 
          ),
          child: _previewImageUrl == null
          ? Center(
            child: Text(
              'No location chosen',
              textAlign: TextAlign.center,
            )
          )
          : Image.network(
            _previewImageUrl,
            fit: BoxFit.cover,
            width: double.infinity
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton.icon(
                icon: Icon(Icons.location_on),
                label: Text(
                  'Current location',
                  style: TextStyle(fontSize: 12.0),
                ),
                textColor: Theme.of(context).accentColor,
                onPressed: _getCurrentUserLocation,
              ),
              FlatButton.icon(
                icon: Icon(Icons.map),
                label: Text(
                  'Select a location',
                  style: TextStyle(fontSize: 12.0)
                ),
                textColor: Theme.of(context).accentColor,
                onPressed: () {
                },
              ),
            ],
          )
        )
      ],)
    );
  }




  Future<void> _getCurrentUserLocation() async {
    final LocationData locData = await Location().getLocation();
    final String staticMapImageUrl = LocationHelper.generateLocationPreviewImage(
      latitude: locData.latitude,
      longitude: locData.longitude
    );

    print(staticMapImageUrl);

    setState(() {
      _previewImageUrl = staticMapImageUrl;
    });
  }

}