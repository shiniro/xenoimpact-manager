import 'package:flutter/material.dart';

class BlockText extends StatelessWidget {
  final Widget child;

  BlockText({
    @required this.child
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: child,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: Color(0xffedf1f2),
      )
    );
  }
}