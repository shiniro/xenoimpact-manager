import 'package:flutter/material.dart';

class UserImage extends StatelessWidget {
  final String imagePath;
  final bool internalImage;

  UserImage({
    this.imagePath,
    this.internalImage
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      width: 80.0,
      child: ClipRRect(
        borderRadius: new BorderRadius.circular(50.0),
        child: internalImage != null && internalImage
          ? Image.asset(
            imagePath,
            fit: BoxFit.fill
          )
          : Image.network(
            imagePath,
            fit: BoxFit.fill
          ),
      )
    );
  }
}