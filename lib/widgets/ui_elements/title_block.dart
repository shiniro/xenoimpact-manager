import 'package:flutter/material.dart';

class TitleBlock extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color iconTextColor;
  final Color background;

  TitleBlock({
    this.title,
    this.icon,
    this.iconTextColor,
    this.background
  });

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    return Container(
      width: targetWidth + 20.0,
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
      decoration: BoxDecoration(
        color: background != null ? background : Color(0xffedf1f2),
      ),
      child: Container(
        child: Row(children: <Widget>[
          icon != null ? Icon(
            icon,
            color: iconTextColor != null ? iconTextColor : Color(0xffaeb8b8),
          ) : Container(),
          icon != null ? SizedBox(width: 10.0) : Container(),
          Text(
            title,
            style: TextStyle(
              color: iconTextColor,
              fontSize: 12.0
            )
          )
        ],)
      )
    );
  }
}