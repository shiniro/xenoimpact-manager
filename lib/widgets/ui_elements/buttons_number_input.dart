import 'package:flutter/material.dart';

class ButtonsNumberInput extends StatefulWidget {
  final String label;
  final double initialValue;
  final Function modifyState;

  ButtonsNumberInput({
    @required this.initialValue,
    @required this.label,
    @required this.modifyState
  });

  @override
  State<StatefulWidget> createState() {
    return _ButtonsNumberInputState();
  }
}

class _ButtonsNumberInputState extends State<ButtonsNumberInput> {
  double _inputValue = 0;
  TextEditingController _controller;

  @override
  void initState() {
    if(widget.initialValue != null) {
      setState(() {
        _inputValue = widget.initialValue;
        _controller = TextEditingController(text: _inputValue.toString());
      });
    }
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(widget.label),
          Container(
            child: Row(children: <Widget>[
              _buildButton('-', decrementValue),
              _buildTextField(),
              _buildButton('+', incrementValue)
            ],),
          )
        ],
      )
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  Widget _buildButton(String signe, Function changeValue) {
    return ButtonTheme (
      minWidth: 50.0,
      height: 50.0,
      child: FlatButton(
        child: Text(
          signe,
          style: TextStyle(
            fontSize: 30.0,
            color: Theme.of(context).textTheme.body1.color
          )
        ),
        shape: new RoundedRectangleBorder(
          side: BorderSide(
            color: Color(0xffd2d5e4),
            width: .5
          )
        ),
        onPressed: () {
          changeValue();
          setState(() {
            widget.modifyState(_inputValue);
            _controller = TextEditingController(text: _inputValue.toString());
          });
        },
      )
    );
  }



  Widget _buildTextField() {
    return Container(
      width: 100.0,
      child: TextField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xffd2d5e4), width: .5),
            borderRadius: BorderRadius.all(Radius.circular(0.0)),
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 13),
        ),
        controller: _controller,
      ),
    );
  }

  /******************************/
  /********** METHODS ***********/ 
  /******************************/

  void incrementValue() {
    setState(() {
      _inputValue++;
    });
  }

  void decrementValue() {
    setState(() {
      _inputValue--;
    });
  }

}