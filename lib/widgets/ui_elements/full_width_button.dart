import 'package:flutter/material.dart';

class FullWidthButton extends StatelessWidget {
  final String text;
  final EdgeInsets padding;
  final Color color;
  final Color textColor;
  final Function onPressed;

  FullWidthButton({
    @required this.text,
    this.padding,
    this.color,
    this.textColor,
    @required this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        padding: padding != null ? padding : EdgeInsets.symmetric(vertical: 14),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: color!= null ? color : Theme.of(context).primaryColor,
        child: Text(
          text.toUpperCase(),
          style: TextStyle(
            fontSize: 14.0,
            color: textColor != null ? textColor : Colors.white
          )
        ),
        onPressed: onPressed
      )
    );
  }
}