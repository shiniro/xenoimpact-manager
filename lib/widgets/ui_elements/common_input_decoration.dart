import 'package:flutter/material.dart';

InputDecoration buildInputDecoration({
    BuildContext context, 
    String placeholder,
    Color labelColor,
    IconData icon,
    Color iconColor,
    EdgeInsets padding,
    Color placeholderColor,
    Color focusColor,
    Color focuseBorderColor,
    Color enabledBorderColor,
    IconData suffixIcon}) {

    return InputDecoration(
      hintText: placeholder,
      labelStyle: labelColor != null ? TextStyle(
        color: labelColor,
        fontSize: 14,
      ) : null,
      prefixIcon: icon != null ? Icon(
        icon,
        color: iconColor != null ? iconColor : Color(0xffd2d5e4),
      ) : null,
      suffixIcon: suffixIcon != null ? Icon(
        suffixIcon,
        color: iconColor != null ? iconColor : Color(0xffd2d5e4),
      ) : null,
      contentPadding: padding != null ? padding : EdgeInsets.symmetric(horizontal: 5.0, vertical: 14),
      hintStyle: TextStyle(
        color: placeholderColor != null ? placeholderColor : Color(0xffb0b9ca),
        fontSize: 14,
      ),
      focusColor: focusColor != null ? focusColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: focuseBorderColor != null ? focuseBorderColor : Color(0xffd2d5e4), width: 1),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: enabledBorderColor != null ? enabledBorderColor : Color(0xffd2d5e4), width: .5),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
    );
  }