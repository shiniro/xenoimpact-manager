import 'package:flutter/material.dart';

import '../../scoped-models/main.dart';

class SwitchListUser extends StatefulWidget {
  final MainModel model;
  final String id;
  final bool initialValue;
  final Function modifyState;

  SwitchListUser({
    @required this.model,
    @required this.id,
    @required this.initialValue,
    @required this.modifyState
  });

  @override
  State<StatefulWidget> createState() {
    return _SwitchListUserState();
  }
}

class _SwitchListUserState extends State<SwitchListUser> {
  bool _switchValue = false;

  @override
  void initState() {
    setState(() {
      _switchValue = widget.initialValue;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      value: _switchValue,
      onChanged: (bool value) {
        setState(() {
          _switchValue = value;
        });
        if(widget.model.selectedUser.id != widget.id)
          widget.model.selectUser(widget.id);
        widget.modifyState(value);
      },
    );
  }
}