/*import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';

import '../helpers/global_translation.dart';
import '../helpers/ensure_visible.dart';

import '../../widgets/ui_elements/common_input_decoration.dart';

class LocationInput extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LocationInputState();
  }
}

class LocationInputState extends State<LocationInput> {
  Uri _staticMapUri;
  final FocusNode _locationInputFocusNode = FocusNode();
  final TextEditingController _locationInputController = TextEditingController();

  @override 
  void initState() {
    _locationInputFocusNode.addListener(_updateLocation); // continue listening even if the widget doesn't exist anymore -> dispose
    getStaticMap();
    super.initState();
  }

  @override
  void dispose() {
    _locationInputFocusNode.removeListener(_updateLocation);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: <Widget>[
        EnsureVisibleWhenFocus(
          focusNode: _locationInputFocusNode,
          child: TextFormField(
            focusNode: _locationInputFocusNode,
            controller: _locationInputController,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: allTranslations.text("manage_company.address"),
              icon: Icons.edit_location,
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Image.network(
          _staticMapUri.toString()
        ),
      ],),
    );
  }




  void getStaticMap() async {
    final StaticMapProvider staticMapProvider = StaticMapProvider('AIzaSyAHmW9KTEOxcHTghZTfP_Iayu5NrPU18FM');
    final Uri staticMapUri = staticMapProvider.getStaticUriWithMarkers(
      [Marker('position', 'Position', 41.40338, 2.17403)],
      center: Location(41.40338, 2.17403),
      width: 500,
      height: 300,
      maptype: StaticMapViewType.roadmap);
    
    setState(() {
      _staticMapUri = staticMapUri;
    });
    
  }



  void _updateLocation() {
    if(!_locationInputFocusNode.hasFocus) {
      getStaticMap();
    }
  }

} */