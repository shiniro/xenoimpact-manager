import 'package:flutter/material.dart';

class CommonSwitch extends StatefulWidget {
  final bool initialValue;
  final Function modifyState;

  CommonSwitch({
    @required this.initialValue,
    @required this.modifyState
  });

  @override
  State<StatefulWidget> createState() {
    return _CommonSwitchState();
  }
}

class _CommonSwitchState extends State<CommonSwitch> {
  bool _switchValue = false;

  @override
  void initState() {
    setState(() {
      _switchValue = widget.initialValue;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: _switchValue,
      onChanged: (bool value) {
        setState(() {
          _switchValue = value;
        });
        widget.modifyState(value);
      },
    );
  }
}