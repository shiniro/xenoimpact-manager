import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:url_launcher/url_launcher.dart';

import '../../models/user.dart';

class ContactFab extends StatefulWidget {
  final User user;

  ContactFab(this.user);

  @override
  State<StatefulWidget> createState() {
    return ContactFabState();
  }
}

class ContactFabState extends State<ContactFab> with TickerProviderStateMixin { // add TickerProviderStateMixin mixin
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200)
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 70.0,
            width: 56.0,
            alignment: FractionalOffset.topCenter,
            child:ScaleTransition(
              scale: CurvedAnimation(
                parent: _controller,
                curve: Interval(0.0, 1.0, curve: Curves.easeOut) // starting point, end point, curve
              ),
              child: FloatingActionButton(
                heroTag: 'email',
                mini: true,
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.mail,
                  color: Theme.of(context).accentColor,
                ),
                onPressed: () async {
                  final url = 'mailto:${widget.user.company.email}?subject=XenoimpactManager';
                  if(await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch !';
                  }
                },
              )
            )
          ),
          Container(
            height: 70.0,
            width: 56.0,
            alignment: FractionalOffset.topCenter,
            child: ScaleTransition(
              scale: CurvedAnimation(
                parent: _controller,
                curve: Interval(0.0, 1.0, curve: Curves.easeOut) // starting point, end point, curve
              ),
              child: FloatingActionButton(
                heroTag: 'phone',
                mini: true,
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.phone,
                  color: Theme.of(context).accentColor,
                ),
                onPressed: () async {
                  final url = 'tel::${widget.user.company.phone}';
                  if(await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch !';
                  }
                },
              )
            )
          ),
          Container(
            child:FloatingActionButton(
              heroTag: 'contacts',
              child: AnimatedBuilder(
                animation: _controller,
                builder: (BuildContext context, Widget child) { // rebuild
                  return Transform(
                    alignment: FractionalOffset.center,
                    transform: Matrix4.rotationZ(_controller.value * .5 * math.pi),
                    child: Icon(_controller.isDismissed ? Icons.more_vert : Icons.close),
                  );
                }
              ), 
              onPressed: () {
                if(_controller.isDismissed) _controller.forward();
                else _controller.reverse();
              },
            )
          )
        ],
      )
    );
  }
}