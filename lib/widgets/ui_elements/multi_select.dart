import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

class MultiSelect extends StatefulWidget {
  final List<String> list;
  final List<String> filter;
  final String type;
  final String page;
  final Function(List<String>) onSelectionChanged;

  MultiSelect({
    Key key,
    this.list,
    this.filter,
    this.type,
    this.page,
    this.onSelectionChanged
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MultiSelectState();
  }
}

class MultiSelectState extends State<MultiSelect> {
  List<String> selectedChoices;

  @override
  void initState() {
    setState(() {
      selectedChoices = List.from(widget.filter);
    });
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Wrap(
        children: _buildChoiceList(),
      ),
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  List<Widget> _buildChoiceList() {
    List<Widget> choices = List();

    widget.list.forEach((item) {
      choices.add(Container(
        padding: EdgeInsets.symmetric(horizontal: 7.0),
        child: ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
          return ChoiceChip(
            label: Text(item),
            labelStyle: TextStyle(
              color: Colors.white
            ),
            selectedColor: _getTagColor(item),
            backgroundColor: Color(0xffb0b0b0),
            selected: selectedChoices.contains(item),
            onSelected: (selected) {
              setState(() {
                if (selectedChoices.contains(item)) {
                  selectedChoices.remove(item);

                  if(widget.page == 'home') {
                    if(widget.type == 'type') model.removeFilterTypeHomepage(item);
                    else model.removeFilterStatusHomepage(item);
                  } else {
                    if(widget.type == 'type') model.removeFilterTypeAdminpage(item);
                    else model.removeFilterStatusAdminpage(item);
                  }

                } else {
                  selectedChoices.add(item);

                  if(widget.page == 'home') {
                    if(widget.type == 'type') model.addFilterTypeHomepage(item);
                    else model.addFilterStatusHomepage(item);
                  } else {
                    if(widget.type == 'type') model.addFilterTypeAdminpage(item);
                    else model.addFilterStatusAdminpage(item);
                  }

                }
                widget.onSelectionChanged(selectedChoices);
              });
            }
          );
        })
      ));
    });

    return choices;
  }


  /******************************/
  /********** METHODS ***********/ 
  /******************************/


  /*
  * GET TAG COLOR
  */
  Color _getTagColor(String tag) {
    if(tag == 'pending') return Color(0xfff18f1b);
    else if (tag == 'approved') return Color(0xff07ad7c);
    else if (tag == 'rejected') return Color(0xffdc4337);
    return Theme.of(context).accentColor;
  }


  /*
  * Reset Filter
  */
  void resetFilter() {
    setState(() {
      selectedChoices = List.from(widget.filter);
    });
  }
}