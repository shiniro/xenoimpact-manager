import 'package:flutter/material.dart';

class TextSeparator extends StatelessWidget {
  final Text text;
  final Color separatorColor;
  final EdgeInsets padding;

  TextSeparator({
    this.text,
    this.separatorColor,
    this.padding
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: <Widget>[
        Expanded(
          child: Divider(
            color: separatorColor
          ),
        ),
        Container(
          padding: padding,
          child: text,
        ),
        Expanded(
          child: Divider(
            color: separatorColor
          ),
        ),
      ],),
    );
  }
}