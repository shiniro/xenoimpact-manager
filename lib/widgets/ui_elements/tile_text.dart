import 'package:flutter/material.dart';

class TileText extends StatelessWidget {
  final String label;
  final String value;

  TileText({
    this.label,
    this.value
  });

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: TextStyle(color: Theme.of(context).textTheme.body1.color),
        children: <TextSpan>[
          TextSpan(
            text: '${label}: ',
          ),
          TextSpan(
            text: value,
            style: TextStyle(color: Color(0xff6c7380))
          )
        ] 
      ),
    );
  }
}