import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

class Tag extends StatelessWidget {
  final String text;
  final Color color;
  final Color textColor;
  final EdgeInsets margin;

  Tag({
    this.text,
    this.color,
    this.textColor,
    this.margin
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      decoration: BoxDecoration(
        color: color != null ? color : Color(0xffb0b0b0),
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      child: Text(
        getTagTitle(text),
        style: TextStyle(
          color: textColor != null ? textColor : Colors.white,
          fontSize: 13.0
        )
      ),
    );
  }

  String getTagTitle(String tag){
    String text;
    switch(tag) {
      case 'pending':
        text = allTranslations.text("pending");
        break;

      case 'approved':
        text = allTranslations.text("approved");
        break;

      case 'rejected':
        text = allTranslations.text("rejected");
        break;
    };

    return text;
  }
}