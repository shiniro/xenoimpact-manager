import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../../widgets/helpers/global_translation.dart';
import '../../models/request.dart';

class ImageInput extends StatefulWidget {
  final Function setImage;
  final Request request;

  ImageInput({this.setImage, this.request});

  @override
  State<StatefulWidget> createState() {
    return _ImageInputState();
  }
}

class _ImageInputState extends State<ImageInput> {
  File _imageFile;

  @override
  Widget build(BuildContext context) {
    Widget previewImage = Text(allTranslations.text('request_form.please_pick_image'));
    if(_imageFile != null) {
      previewImage = Image.file(
        _imageFile,
        fit: BoxFit.cover,
        height: 300.0,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topCenter,
      );
    } else if(widget.request != null && widget.request.fileUrl != null) {
      previewImage = Image.network(
        widget.request.fileUrl,
        fit: BoxFit.cover,
        height: 300.0,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topCenter
      );
    }

    return Container(
      child: Column(children: <Widget>[
        OutlineButton(
          onPressed: () {
            _openImagePicker(context);
          },
          borderSide: BorderSide(color: Theme.of(context).accentColor),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.camera_alt,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(width: 5.0),
              Text(
                widget.request != null ? allTranslations.text('request_form.change_image') : allTranslations.text('request_form.add_image'),
                style: TextStyle(
                  color: Theme.of(context).accentColor
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10.0),
        previewImage
      ],)
    );
  }


  void _openImagePicker(BuildContext context) {
    showModalBottomSheet(context: context, builder: (BuildContext context) {
      return Container(
        height: 150.0,
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              allTranslations.text('request_form.pick_image'),
              style: TextStyle(fontWeight: FontWeight.bold)
            ),
            SizedBox(height: 10.0),
            FlatButton(
              textColor: Theme.of(context).accentColor,
              child: Text(allTranslations.text('request_form.use_camera')),
              onPressed: () {
                _getImage(context, ImageSource.camera);
              },
            ),
            FlatButton(
              textColor: Theme.of(context).accentColor,
              child: Text(allTranslations.text('request_form.use_galery')),
              onPressed: () {
                _getImage(context, ImageSource.gallery);
              },
            ),
          ],
        )
      );
    });
  }



  void _getImage(BuildContext context, ImageSource source) {
    ImagePicker.pickImage(source: source, maxWidth: 400.0).then((File image) {
      setState(() {
        _imageFile = image;
      });
      widget.setImage(image);
      Navigator.pop(context);
    });
  }
}