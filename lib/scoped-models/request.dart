import 'dart:io';
import 'dart:convert'; // convert/decode json
import 'dart:async';
import 'package:http/http.dart' as http; // http request
import 'package:intl/intl.dart'; // for DateFormat

import '../models/request.dart';

import './connected_data.dart';

class RequestModel extends ConnectedDataModel {

  List <Request> _requests = [];
  String _selfRequestId;


  /************ Getters *************/

  /*
  * GET all requests
  */ 
  List<Request> get allRequests {
    return List.from(_requests);
  }

  /*
  * GET all requests by Id
  */
  List<Request> get allRequestsByUserId {
    return _requests.where((Request request) {
      return request.user == authenticatedUser.id;
    });
  }

  /*
  * GET request by id - selelected request
  */
  Request get selectedRequest {
    if(selectedRequestId == null) {
      return null;
    }
    return _requests.firstWhere((Request request) {
      return request.id == _selfRequestId;
    });
  }

  /*
  * GET selected request id
  */
  String get selectedRequestId {
    return _selfRequestId;
  }

  /*
  * GET selected request index
  */
  int get selectedRequestIndex {
    return _requests.indexWhere((Request request) {
      return request.id == _selfRequestId;
    });
  }

  /*
  * GET selected request status
  */
  String get selectedRequestStatus {
    return selectedRequest.status;
  }

  /*
  * GET selected request comment
  */
  String get selectedRequestComment {
    return selectedRequest.comment;
  }



  /************ Function *************/

  /*
  * Select Request
  * 
  */
  void selectRequest(String requestId) {
    _selfRequestId = requestId;
    notifyListeners();
  }




  /*
  * Get all request by status and type
  * 
  */
  List<Request> allRequestByStatusAndType(String status, String type, DateTime startDate, DateTime endDate, double nbDays, [String page]) {
    return List.from(_requests.where((Request request) {
      return 
        (status != null ? request.status == status && adminPageStatusFilter.contains(request.status) : request.status != 'pending' && adminPageStatusFilter.contains(request.status))
        && (type != null ? request.type == type && adminPageTypeFilter.contains(request.type) : adminPageTypeFilter.contains(request.type))
        && (startDate != null ? startDate.isBefore(request.startDate) || startDate == request.startDate : request.startDate != null)
        && (endDate != null ? endDate.isAfter(request.endDate) || endDate == request.startDate : request.endDate != null)
        && (nbDays != null ? nbDays == request.nbDays : request.nbDays != null);
    } ));
  }




  /*
  * Get all reqyest by status, type and userid
  * 
  */
  List<Request> allRequestByStatusAndTypeAndUserId(String status, String type, DateTime startDate, DateTime endDate, double nbDays, [String page]) {
    return List.from(_requests.where((Request request) {
      return 
        (status != null ? request.status == status && homePageStatusFilter.contains(request.status) : request.status != 'pending' && homePageStatusFilter.contains(request.status))
        && (type != null ? request.type == type && homePageTypeFilter.contains(request.type) : homePageTypeFilter.contains(request.type))
        && request.user == authenticatedUser.id
        && (startDate != null ? startDate.isBefore(request.startDate) || startDate == request.startDate : request.startDate != null)
        && (endDate != null ? endDate.isAfter(request.endDate) || endDate == request.startDate : request.endDate != null)
        && (nbDays != null ? nbDays == request.nbDays : request.nbDays != null);
    } ));
  }



  /************ Http Request *************/

  /*
  * Fetch Request Leave
  * 
  */
  Future<dynamic> fetchRequests([String companyId]) async {
    isLoadingData = true;
    notifyListeners();

    // get all requests
    http.Response response;
    if(companyId != null ) {
      response = await http.get(
        '${apiUrl}/requests/company/${companyId}',
        headers: headers
      );
    } else {
      response = await http.get(
        '${apiUrl}/requests',
        headers: headers
      );
    }
    final List<Request> fetchedRequestList = [];

    //handle response
    if(response.body != null) {

      final Map<String, dynamic> requestsListData = json.decode(response.body);
      if(requestsListData == null) {
        isLoadingData = false;
        notifyListeners();
        return;
      }

      print(requestsListData);

      // store the requests
      Set.from(requestsListData['requests']).forEach((dynamic requestData) {
        if (requestData != null && !(requestData is int)) {
          final Request request = Request(
            id: requestData['_id'],
            endDate: requestData['endDate'] is String ? DateFormat('yyyy/MM/dd', "en_US").parse(requestData['endDate']) : requestData['endDate'],
            startDate: requestData['startDate'] is String ? DateFormat('yyyy/MM/dd', "en_US").parse(requestData['startDate']) : requestData['startDate'],
            status: requestData['status'],
            type: requestData['type'],
            user: requestData['user']['_id'],
            userName: requestData['user']['name'],
            nbDays: requestData['nbDays'].toDouble(),
            comment: requestData['comment'],
            fileUrl: requestData["fileUrl"] != null ? '${apiUrl}/${requestData["fileUrl"]}' : null
          );
          fetchedRequestList.add(request);
        }
      });
      _requests = fetchedRequestList;

      isLoadingData = false;
      notifyListeners();
    }
  }



  /*
  * Add Request Leave
  * register user
  */
  Future<bool> addRequestLeave(String startDate, String endDate, double nbDays, String type, [File image]) async {

    isLoadingData = true;
    notifyListeners();

    // change date format to YYYY/MM/dd
    if(!startDate.contains('/')) {
      startDate = startDate.substring(0, 4) + '/' + startDate.substring(4, 6) + '/' + startDate.substring(6, startDate.length);
    }
    if(!endDate.contains('/')) {
      endDate = endDate.substring(0, 4) + '/' + endDate.substring(4, 6) + '/' + endDate.substring(6, endDate.length);
    }

    final request = http.MultipartRequest(
        'POST', 
        Uri.parse('${apiUrl}/requests/'));

    request.fields['startDate'] = startDate;
    request.fields['endDate'] = endDate;
    request.fields['nbDays'] = nbDays.toString();
    request.fields['type'] = type;
    request.fields['status'] = 'pending';
    request.fields['user'] = authenticatedUser.id;
    request.headers['Authorization'] = 'Bearer ${authenticatedUser.token}';
    request.headers['Content-Encoding'] = "application/json";

    if(image != null) {
      var pic = await http.MultipartFile.fromPath("fileUrl", image.path);
      request.files.add(pic);
    } 

    // request - create request
    try {
      var response = await request.send();
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      final Map<String, dynamic> responseDataObject = json.decode(responseString);

      // handle response
      if(response.statusCode != 200 && response.statusCode != 201) { // error
        print(responseDataObject['error']);
        return false;

      } else { // success
        // update user data -> change number of days off, days off taken, summer vacation, etc
        /* Map<String, dynamic> userData = await updateUserData(
          id: authenticatedUser.id,
          token: authenticatedUser.token,
          daysOff: type == 'day off' ? authenticatedUser.daysOff - nbDays : authenticatedUser.daysOff,
          summerVacation: type == 'summer vacation' ? authenticatedUser.summerVacation - nbDays : authenticatedUser.summerVacation,
          medicalLeaveTaken: type == 'medical leave' ? authenticatedUser.medicalLeaveTaken + nbDays : authenticatedUser.medicalLeaveTaken,
          daysOffTaken: type == 'day off' ? authenticatedUser.daysOffTaken + nbDays : authenticatedUser.daysOffTaken,
          summerVacationTaken: type == 'summer vacation' ? authenticatedUser.summerVacationTaken + nbDays : authenticatedUser.summerVacationTaken,
          daysOffNoPaidTaken: type == 'no paid' ? authenticatedUser.daysOffNoPaidTaken + nbDays : authenticatedUser.daysOffNoPaidTaken,
        ); */

        // handle response (update user)
        // if(userData != null && responseData != null) { // success

          // create new request to store
          final Request newRequest = Request(
            id: responseDataObject['createdRequest']['_id'],
            nbDays: nbDays,
            startDate: DateFormat('yyyy/MM/dd', "en_US").parse(startDate),
            endDate: DateFormat('yyyy/MM/dd', "en_US").parse(endDate),
            type: type,
            status: 'pending',
            user: authenticatedUser.id,
            userName: authenticatedUser.name,
            fileUrl: responseDataObject["createdRequest"]["fileUrl"] != null ? '${apiUrl}/${responseDataObject["createdRequest"]["fileUrl"]}' : null
          );

          _requests.add(newRequest); // add request to list of request in the store
          isLoadingData = false;
          notifyListeners();
          return true;
        // }
      }
    } catch (error) {
      isLoadingData = false;
      notifyListeners();
      return false;
    }
  }



  /*
  * Update Request Leave
  * 
  */
  Future<bool> updateRequestLeave(String startDate, String endDate, double nbDays, String type, [File image]) async {

    isLoadingData = true;
    notifyListeners();

    // change date format to YYYY/MM/dd
    if(!startDate.contains('/')) {
      startDate = startDate.substring(0, 4) + '/' + startDate.substring(4, 6) + '/' + startDate.substring(6, startDate.length);
    }
    if(!endDate.contains('/')) {
      endDate = endDate.substring(0, 4) + '/' + endDate.substring(4, 6) + '/' + endDate.substring(6, endDate.length);
    }

    final request = http.MultipartRequest(
        'PUT', 
        Uri.parse('${apiUrl}/requests/${selectedRequest.id}'));

    request.fields['startDate'] = startDate;
    request.fields['endDate'] = endDate;
    request.fields['nbDays'] = nbDays.toString();
    request.fields['type'] = type;
    request.fields['status'] = 'pending';
    request.fields['user'] = authenticatedUser.id;
    request.headers['Authorization'] = 'Bearer ${authenticatedUser.token}';
    request.headers['Content-Encoding'] = "application/json";

    if(image != null) {
      var pic = await http.MultipartFile.fromPath("fileUrl", image.path);
      request.files.add(pic);
    } 

    // if the request type changed
    if(selectedRequest.type != type) {

      // update user data 
      updateUserData(
        id: authenticatedUser.id,
        token: authenticatedUser.token,
        daysOff: 
          selectedRequest.type == 'day off' 
            ? authenticatedUser.daysOff - selectedRequest.nbDays 
            : type == 'day off'
              ? authenticatedUser.daysOff > nbDays ? authenticatedUser.daysOff - nbDays : authenticatedUser.daysOff + nbDays
              : null,
        summerVacation: 
          selectedRequest.type == 'summer vacation' 
            ? authenticatedUser.summerVacation - selectedRequest.nbDays 
            : type == 'summer vacation'
              ? authenticatedUser.summerVacation > nbDays  ? authenticatedUser.summerVacation - nbDays : authenticatedUser.summerVacation + nbDays 
              : null,
        medicalLeaveTaken: 
          selectedRequest.type == 'medical leave' 
            ? authenticatedUser.medicalLeaveTaken + selectedRequest.nbDays 
            : type == 'medical leave'
              ? authenticatedUser.medicalLeaveTaken > nbDays ? authenticatedUser.medicalLeaveTaken - nbDays : authenticatedUser.medicalLeaveTaken + nbDays
              : null,
        daysOffTaken: 
          selectedRequest.type == 'day off' 
            ? authenticatedUser.daysOffTaken + selectedRequest.nbDays 
            : type == 'day off'
              ? authenticatedUser.daysOffTaken > nbDays ? authenticatedUser.daysOffTaken + nbDays : authenticatedUser.daysOffTaken - nbDays 
              : null,
        summerVacationTaken: 
          selectedRequest.type == 'summer vacation' 
            ? authenticatedUser.summerVacationTaken + selectedRequest.nbDays 
            : type == 'summer vacation'
              ? authenticatedUser.summerVacationTaken > nbDays ? authenticatedUser.summerVacationTaken + nbDays : authenticatedUser.summerVacationTaken - nbDays 
              : null,
        daysOffNoPaidTaken: 
          selectedRequest.type == 'no paid' 
            ? authenticatedUser.daysOffNoPaidTaken + selectedRequest.nbDays 
            : type == 'no paid'
              ? authenticatedUser.daysOffNoPaidTaken > nbDays ? authenticatedUser.daysOffNoPaidTaken + nbDays : authenticatedUser.daysOffNoPaidTaken - nbDays 
              : null
      );
    } else if (selectedRequest.type == type && nbDays != selectedRequest.nbDays) { // if request type didn't change but number of days change

      // calculate new number of days
      double daysNumber = nbDays;
      if (nbDays != selectedRequest.nbDays) {
        daysNumber = nbDays - selectedRequest.nbDays;
      }

      // update user data
      updateUserData(
        id: authenticatedUser.id,
        token: authenticatedUser.token,
        daysOff: type == 'day off' ? authenticatedUser.daysOff + daysNumber : authenticatedUser.daysOff,
        summerVacation: type == 'summer vacation' ? authenticatedUser.summerVacation + daysNumber : authenticatedUser.summerVacation,
        medicalLeaveTaken: type == 'medical leave' ? authenticatedUser.medicalLeaveTaken + daysNumber : authenticatedUser.medicalLeaveTaken,
        daysOffTaken: type == 'day off' ? authenticatedUser.daysOffTaken - daysNumber : authenticatedUser.daysOffTaken - daysNumber,
        summerVacationTaken: type == 'summer vacation' ? authenticatedUser.summerVacationTaken - daysNumber : authenticatedUser.summerVacationTaken,
        daysOffNoPaidTaken: type == 'no paid' ? authenticatedUser.daysOffNoPaidTaken - daysNumber : authenticatedUser.daysOffNoPaidTaken,
      );
    }

    // request
    try {
      var response = await request.send();
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      final Map<String, dynamic> responseDataObject = json.decode(responseString);

      // handle request response
      if (response.statusCode == 200) {
        
        isLoadingData = false;

        String imageUrl;
        if(image == null) // get image info
          imageUrl = selectedRequest.fileUrl;
        else 
          imageUrl = responseDataObject["createdRequest"]["fileUrl"] != null ? '${apiUrl}/${responseDataObject["createdRequest"]["fileUrl"]}' : null;

        // create a new request
        final Request newRequest = Request(
          id: selectedRequest.id,
          nbDays: nbDays,
          startDate: DateFormat('yyyy/MM/dd', "en_US").parse(startDate),
          endDate: DateFormat('yyyy/MM/dd', "en_US").parse(endDate),
          type: type,
          status: selectedRequest.status,
          user: selectedRequest.user,
          userName: selectedRequest.userName,
          fileUrl: imageUrl
        );

        // replace old request by the new request
        _requests[selectedRequestIndex] = newRequest;

        notifyListeners();
        return true;
      }
    } catch(error) {
      isLoadingData = false;
      notifyListeners();
      return false;
    }

    return false;
  }



  /*
  * Update Status
  * 
  */
  Future<bool> updateRequestStatus(String status, [String comment]) async {
    isLoadingData = true;
    notifyListeners();

    // request data
    final Map<String, dynamic> requestData = {
      'status': status,
      'comment': comment != null ? comment : null,
    };

    // update request
    return await http.put(
      '${apiUrl}/requests/${selectedRequest.id}',
      body: json.encode(requestData),
      headers: headers
    ).then((http.Response response) {
      isLoadingData = false;

      // update user data
      if(status == 'rejected') {
        updateUserData(
          id: authenticatedUser.id,
          token: authenticatedUser.token,
          daysOff: selectedRequest.type == 'day off' ? authenticatedUser.daysOff + selectedRequest.nbDays : authenticatedUser.daysOff,
          summerVacation: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacation + selectedRequest.nbDays : authenticatedUser.summerVacation,
          medicalLeaveTaken: selectedRequest.type == 'medical leave' ? authenticatedUser.medicalLeaveTaken - selectedRequest.nbDays : authenticatedUser.medicalLeaveTaken,
          daysOffTaken: selectedRequest.type == 'day off' ? authenticatedUser.daysOffTaken - selectedRequest.nbDays : authenticatedUser.daysOffTaken,
          summerVacationTaken: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacationTaken - selectedRequest.nbDays : authenticatedUser.summerVacationTaken,
          daysOffNoPaidTaken: selectedRequest.type == 'no paid' ? authenticatedUser.daysOffNoPaidTaken - selectedRequest.nbDays : authenticatedUser.daysOffNoPaidTaken
        );
      } else if(selectedRequest.status == 'rejected') {
        updateUserData(
          id: authenticatedUser.id,
          token: authenticatedUser.token,
          daysOff: selectedRequest.type == 'day off' ? authenticatedUser.daysOff - selectedRequest.nbDays : authenticatedUser.daysOff,
          summerVacation: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacation - selectedRequest.nbDays : authenticatedUser.summerVacation,
          medicalLeaveTaken: selectedRequest.type == 'medical leave' ? authenticatedUser.medicalLeaveTaken + selectedRequest.nbDays : authenticatedUser.medicalLeaveTaken,
          daysOffTaken: selectedRequest.type == 'day off' ? authenticatedUser.daysOffTaken + selectedRequest.nbDays : authenticatedUser.daysOffTaken,
          summerVacationTaken: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacationTaken + selectedRequest.nbDays : authenticatedUser.summerVacationTaken,
          daysOffNoPaidTaken: selectedRequest.type == 'no paid' ? authenticatedUser.daysOffNoPaidTaken + selectedRequest.nbDays : authenticatedUser.daysOffNoPaidTaken,
        );
      }


      // create new request
      final Request newRequest = Request(
        id: selectedRequest.id,
        nbDays: selectedRequest.nbDays,
        startDate: selectedRequest.startDate,
        endDate: selectedRequest.endDate,
        type: selectedRequest.type,
        status: status,
        user: selectedRequest.user,
        userName: selectedRequest.userName,
        comment: comment != null ? comment : null,
        fileUrl: selectedRequest.fileUrl,
        filePath: selectedRequest.filePath
      );

      // replace request by the new one
      _requests[selectedRequestIndex] = newRequest;
      notifyListeners();
      return true;

    }).catchError((error){ // error 

      isLoadingData = false;
      notifyListeners();
      return false;
      
    });
  }




  /*
  * Delete Request Leave
  * 
  */
  Future<bool> deleteRequestLeave() async {
    isLoadingData = true;
    final deletedRequestId = selectedRequest.id;

    // update user data
    if(selectedRequest.status != 'rejected') {
      updateUserData(
        id: authenticatedUser.id,
        token: authenticatedUser.token,
        daysOff: selectedRequest.type == 'day off' ? authenticatedUser.daysOff + selectedRequest.nbDays : authenticatedUser.daysOff,
        summerVacation: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacation + selectedRequest.nbDays : authenticatedUser.summerVacation,
        medicalLeaveTaken: selectedRequest.type == 'medical leave' ? authenticatedUser.medicalLeaveTaken - selectedRequest.nbDays : authenticatedUser.medicalLeaveTaken,
        daysOffTaken: selectedRequest.type == 'day off' ? authenticatedUser.daysOffTaken - selectedRequest.nbDays : authenticatedUser.daysOffTaken,
        summerVacationTaken: selectedRequest.type == 'summer vacation' ? authenticatedUser.summerVacationTaken - selectedRequest.nbDays : authenticatedUser.summerVacationTaken,
        daysOffNoPaidTaken: selectedRequest.type == 'summer vacation' ? authenticatedUser.daysOffNoPaidTaken - selectedRequest.nbDays : authenticatedUser.daysOffNoPaidTaken
      );
    }

    // remove request in the store
    _requests.removeAt(selectedRequestIndex);
    selectRequest(null);
    notifyListeners();

    // delete request
    http
      .delete(
        '${apiUrl}/requests/${deletedRequestId}',
        headers: headers
      )
      .then((http.Response response) { // success
        isLoadingData = false;
        notifyListeners();
        return true;
      })
      .catchError((error) { // error
        isLoadingData = false;
        notifyListeners();
        return false;
      });

    return false;
  }
}