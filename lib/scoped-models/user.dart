import 'dart:convert'; // convert/decode json
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http; // http request
import 'package:shared_preferences/shared_preferences.dart'; // cookies
import 'package:intl/intl.dart'; // for DateFormat
import 'package:rxdart/rxdart.dart';
import '../widgets/helpers/global_translation.dart';

import './connected_data.dart';
import '../models/user.dart';
import '../models/company.dart';
import '../models/vacation_rule.dart';

class UserModel extends ConnectedDataModel {

  List <User> _users = [];
  String _selfUserId;

  Timer _authTimer;
  PublishSubject<bool> _userSubject = PublishSubject();

  final GoogleSignIn _googleSignIn = GoogleSignIn();

  /************ Getters *************/

  /*
  * GET all users
  */ 
  List<User> get allUsers {
    return List.from(_users);
  }


  /*
  * GET user by id - selelected user
  */
  User get selectedUser {
    if(selectedUserId == null) {
      return null;
    }
    return _users.firstWhere((User user) {
      return user.id == _selfUserId;
    });
  }



  /*
  * GET selected user id
  */
  String get selectedUserId {
    return _selfUserId;
  }

  /*
  * GET selected user index
  */
  int get selectedUserIndex {
    return _users.indexWhere((User user) {
      return user.id == _selfUserId;
    });
  }


  /*
  * GET User
  */
  User get user {
    return authenticatedUser;
  }

  /*
  * GET UserID
  */
  String get userID {
    return authenticatedUser.id;
  }

  /*
  * GET UserSubject
  */
  PublishSubject<bool> get userSubject {
    return _userSubject;
  }



  /************ Function *************/

  /*
  * Select user
  * 
  */
  void selectUser(String userId) {
    _selfUserId = userId;
    notifyListeners();
  }




  /************ Http Request *************/


  /*
  * Fetch Request Leave
  * 
  */
  Future<dynamic> fetchUsers([String companyId]) async {
    isLoadingData = true;
    notifyListeners();

    // get all requests
    http.Response response;
    if(companyId != null) {
      response = await http.get(
        '${apiUrl}/user/company/${companyId}',
        headers: headers
      );
    } else {
      response = await http.get(
        '${apiUrl}/user',
        headers: headers
      );
    }
    final List<User> fetchedUserList = [];

    //handle response
    if(response.body != null) {

      final Map<String, dynamic> fetchedUserListData = json.decode(response.body);
      if(fetchedUserListData == null) {
        isLoadingData = false;
        notifyListeners();
        return;
      }


      // store the requests
      if(fetchedUserListData['users'] != null) {
        Set.from(fetchedUserListData['users']).forEach((dynamic userData) {
          if (userData != null && !(userData is int)) {
            final User newUser = User(
              id: userData['_id'],
              email: userData['email'],
              name: userData['name'],
              pictureUrl: userData['pictureUrl'],
              company: Company(
                id: userData['company']['_id'],
                name: userData['company']['name'],
                email: userData['company']['email'],
                emailBase: userData['company']['emailBase'],
                phone: userData['company']['phone'],
                language: userData['company']['language'],
                address: userData['company']['address'],
                country: userData['company']['country'],
                vacationRule: userData['company']['vacationRule'],
                defaultRule: userData['company']['defaultRule']
              ),
              vacationRule: VacationRule(
                id: userData['vacationRule']['id'],
                name: userData['vacationRule']['name'],
                autoActionOnChangeYear: userData['vacationRule']['autoActionOnChangeYear'],
                resetOnChangeYear: userData['vacationRule']['resetOnChangeYear'],
                nbDaysOffPerYear: userData['vacationRule']['nbDaysOffPerYear'] != null ? userData['vacationRule']['nbDaysOffPerYear'].toDouble() : null,
                nbSummerVacationPerYear: userData['vacationRule']['nbSummerVacationPerYear'] != null ? userData['vacationRule']['nbSummerVacationPerYear'].toDouble() : null,
                resetDate: userData['vacationRule']['resetDate'],
                addEachMonth: userData['vacationRule']['addEachMonth'] || userData['vacationRule']['addEachMonth'] == 'true',
                nbDayAddEachMonth: userData['vacationRule']['nbDayAddEachMonth'],
                summerVacationAddDate: userData['vacationRule']['summerVacationAddDate']
              ),
              os: userData['os'],
              pushId: userData['pushId'],
              token: userData['token'],
              admin: userData['admin'] == 'true' || userData['admin'],
              superAdmin: userData['superAdmin'] || userData['superAdmin'] == 'true',
              daysOff: userData['daysOff'].toDouble(),
              summerVacation: userData['summerVacation'].toDouble(),
              summerVacationTaken: userData['summerVacationTaken'].toDouble(),
              daysOffNoPaidTaken: userData['daysOffNoPaidTaken'].toDouble(),
              daysOffTaken: userData['daysOffTaken'].toDouble(),
              medicalLeaveTaken: userData['medicalLeaveTaken'].toDouble(),
              language: userData['language'],
              lastSignUp: userData['lastSignUp'],
              expiryTime: userData['expiryTime'],
              autoActionOnChangeYear: userData['autoActionOnChangeYear'] == 'true' || userData['autoActionOnChangeYear'],
              resetOnChangeYear: userData['resetOnChangeYear'] == 'true' || userData['resetOnChangeYear']
            );
            fetchedUserList.add(newUser);
          }
        });
        _users = fetchedUserList;
      }

      isLoadingData = false;
      notifyListeners();
    }
  }




  /*
  * Login with Google
  * login + save the user + create cookie
  */
  Future<Map<String, dynamic>> signInWithGoogle(String pushId, String os) async {
    isLoadingData = true;
    notifyListeners();

    // Get google user
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

     // Handle request response
    bool hasError = true;
    String message ='Something went wrong';

    // get user if exist
    http.Response response = await http.get(
      '${apiUrl}/user/email/${googleUser.email}',
      headers: headers
    );
    final Map<String, dynamic> user = json.decode(response.body)['user'];
    Map<String, dynamic> loginResponse;

    // if user doesn't exist - create user
    if((user == null || user.length <= 0) && response.statusCode != 401) {

      //User data
      final Map<String, dynamic> requestData = {
        'email': googleUser.email,
        'name': googleUser.displayName,
        'password': googleUser.id,
        'pictureUrl': googleUser.photoUrl,
        'os': os,
        'pushId': pushId,
        'language': allTranslations.currentLanguage
      };
      
      // create user
      http.Response response = await http.post(
        '${apiUrl}/user/signup',
        body: json.encode(requestData),
        headers: headers
      );
      final Map<String, dynamic> responseData = json.decode(response.body);
      
      //handle response
      if(response.statusCode == 201) { //registered

        //login
        print('googleUser.id');
        print(googleUser.id);
        loginResponse = await signIn(googleUser.email, googleUser.id, pushId, os);

        //handle login response
        if(loginResponse['success']) hasError = false;
        else message = loginResponse['message'];

      } else { //error
        message = responseData['error'];
      }
    } else if(response.statusCode == 401) {
      print('googleUser.id');
      print(googleUser.id);
      message = 'Wrong password';
      print(message);
    } else { // user exist

      //login
      loginResponse = await signIn(googleUser.email, googleUser.id, pushId, os);

      //handle login response
      if(loginResponse['success']) hasError = false;
      else message = loginResponse['message'];
    }

    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }



  /*
  * Login
  * 
  */
  Future<Map<String, dynamic>> signIn(String email, String password, String pushId, String os) async {
    isLoadingData = true;
    notifyListeners();

    // authentification data
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password
    };

    // login request
    http.Response response = await http.post(
      '${apiUrl}/user/login',
      body: json.encode(authData),
      headers: headers
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    print('login');
    print(responseData);

    // Handle request response
    bool hasError = true;
    String message ='Something went wrong';

    // success -> get a token
    if (responseData.containsKey('token')) {
      headers['Authorization'] = 'Bearer ${responseData["token"]}'; //change headers for autorization
      hasError = false;
      message = 'Authentification succeeded';

      // get user data
      Map<String, dynamic> user = responseData['user'];
      DateTime lastSignUp;

      // if lastLogin exist - get lastLogin
      if(user['lastLogin'] != null) {
        if(os == 'iOs') {
          lastSignUp = DateTime.fromMillisecondsSinceEpoch(user['lastLogin'] * 1000, isUtc: true);
        } else {
          lastSignUp = DateTime.fromMillisecondsSinceEpoch(user['lastLogin']);
        }
      }

      // check if need to reset day off and vacation 
      /* if(user != null && user["id"] != null && lastSignUp != null && lastSignUp.year != today.year && user['autoActionOnChangeYear'] == 'true') {
        // new day off and vacation
        final Map<String, dynamic> updateUserData = {
          'summerVacation': today.month >= 6 ? summerVacationInit : 0,
          'daysOff': user['resetOnChangeYear'] == 'true' ? today.month.toDouble() : user['daysOff'] + today.month.toDouble(),
          'daysOffTaken': 0,
          'summerVacationTaken' : 0,
          'daysOffNoPaidTaken' : 0,
          'medicalLeaveTaken': 0
        };

        // modify user data
        http.Response response = await http.put(
          '${apiUrl}/user/${user["id"]}',
          body: json.encode(updateUserData),
          headers: headers
        );

        // error
        if(response.statusCode != 200) {
          return {'message': 'Reset day off error', 'success': true};
        }
      } */

      // change app language depend on the user language
      if(user['language'] != allTranslations.currentLanguage)
        await allTranslations.setNewLanguage(user['language']);

        print(user['company']);


      // store user
      authenticatedUser = User(
        id: user['_id'],
        email: email,
        password: password,
        name: user['name'],
        pictureUrl: user['pictureUrl'],
        company: Company(
          id: user['company']['_id'],
          name: user['company']['name'],
          email: user['company']['email'],
          emailBase: user['company']['emailBase'],
          phone: user['company']['phone'],
          language: user['company']['language'],
          address: user['company']['address'],
          country: user['company']['country'],
          vacationRule: user['company']['vacationRule'],
          defaultRule: user['company']['defaultRule']
        ),
        vacationRule: VacationRule(
          id: user['vacationRule']['id'],
          name: user['vacationRule']['name'],
          autoActionOnChangeYear: user['vacationRule']['autoActionOnChangeYear'],
          resetOnChangeYear: user['vacationRule']['resetOnChangeYear'],
          nbDaysOffPerYear: user['vacationRule']['nbDaysOffPerYear'] != null ? user['vacationRule']['nbDaysOffPerYear'].toDouble() : null,
          nbSummerVacationPerYear: user['vacationRule']['nbSummerVacationPerYear']!= null ? user['vacationRule']['nbSummerVacationPerYear'].toDouble() : null,
          resetDate: user['vacationRule']['resetDate'],
          addEachMonth: user['vacationRule']['addEachMonth'] || user['vacationRule']['addEachMonth'] == 'true',
          nbDayAddEachMonth: user['vacationRule']['nbDayAddEachMonth'] != null ? user['vacationRule']['nbDayAddEachMonth'].toDouble() : null,
          summerVacationAddDate: user['vacationRule']['summerVacationAddDate']
        ),
        os: os,
        pushId: pushId,
        token: responseData['token'],
        admin: user['admin'] || user['admin'] == 'true',
        superAdmin: user['superAdmin'] != null && (user['superAdmin'] || user['superAdmin'] == 'true'),
        daysOff: user['daysOff'].toDouble(),
        summerVacation: user['summerVacation'].toDouble(),
        summerVacationTaken: user['summerVacationTaken'].toDouble(),
        daysOffNoPaidTaken: user['daysOffNoPaidTaken'].toDouble(),
        daysOffTaken: user['daysOffTaken'].toDouble(),
        medicalLeaveTaken: user['medicalLeaveTaken'].toDouble(),
        nbDaysOffPerYear: user['nbDaysOffPerYear'] != null ? user['nbDaysOffPerYear'].toDouble() : null,
        nbSummerVacationPerYear: user['nbSummerVacationPerYear'] != null ? user['nbSummerVacationPerYear'].toDouble() : null,
        lastSignUp: lastSignUp,
        language: user['language'],
        autoActionOnChangeYear: user['autoActionOnChangeYear'] == 'true' || user['autoActionOnChangeYear'],
        resetOnChangeYear: user['resetOnChangeYear'] == 'true' || user['resetOnChangeYear'],
        expiryTime: user['expiryTime']
      );

      if(user['expiryTime'] != null) {
        setAuthTimeOut(user['expiryTime']);
      }

      _userSubject.add(true); 

      // cookies 
      final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
      prefs.setString('token', responseData['token']);
      prefs.setString('id', user['_id']);
      prefs.setString('name', user['name']);
      prefs.setString('email', email);
      prefs.setString('password', password);
      prefs.setString('pictureUrl', user['pictureUrl']);
      prefs.setString('company', json.encode(user['company']));
      prefs.setString('vacationRule', json.encode(user['vacationRule']));
      prefs.setString('os', os);
      prefs.setString('pushId', pushId);
      prefs.setBool('admin', user['admin']);
      prefs.setBool('superAdmin', user['superAdmin']);
      prefs.setDouble('daysOff', user['daysOff'].toDouble());
      prefs.setDouble('summerVacation', user['summerVacation'].toDouble());
      prefs.setDouble('summerVacationTaken', user['summerVacationTaken'].toDouble());
      prefs.setDouble('daysOffNoPaidTaken', user['daysOffNoPaidTaken'].toDouble());
      prefs.setDouble('daysOffTaken', user['daysOffTaken'].toDouble());
      prefs.setDouble('medicalLeaveTaken', user['medicalLeaveTaken'].toDouble());
      prefs.setDouble('nbDaysOffPerYear', user['nbDaysOffPerYear'] != null ? user['nbDaysOffPerYear'].toDouble() : null);
      prefs.setDouble('nbSummerVacationPerYear', user['nbSummerVacationPerYear'] != null ? user['nbSummerVacationPerYear'].toDouble() : null);
      if(lastSignUp != null) prefs.setString('lastSignUp', DateFormat("yyyy/MM/dd").format(lastSignUp));
      prefs.setInt('expiryTime', user['expiryTime']);
      prefs.setString('language', user['language']);
      prefs.setBool('autoActionOnChangeYear', user['autoActionOnChangeYear'] == 'true' || user['autoActionOnChangeYear']);
      prefs.setBool('resetOnChangeYear', user['resetOnChangeYear'] == 'true' || user['resetOnChangeYear']);

      isLoadingData = false;
      notifyListeners();

    } else if(response.statusCode != 200) { //error
      message = responseData['message'];
    } 

    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }




  /*
  * Fetch User data
  * Get user data by id
  */
  Future<Map<String, dynamic>> getUserData(String id) async {
    try {
      http.Response response = await http.get(
        '${apiUrl}/user/${id}',
        headers: headers
      );

      print(response.body);

      if(response != null) {
        return json.decode(response.body);
      }
      return null;

    } catch (error) {
      print(error);
      return null;
    }
  }




  /*
  * Register
  * register user
  */
  Future<Map<String, dynamic>> register(String email, String password, String name,  String pushId, String os) async {
    isLoadingData = true;
    notifyListeners();

    // new User data
    final Map<String, dynamic> requestData = {
      'email': email,
      'name': name,
      'password': password,
      'os': os,
      'pushId': pushId,
      'language': allTranslations.currentLanguage,
    };
    
    // create user
    http.Response response = await http.post(
      '${apiUrl}/user/signup',
      body: json.encode(requestData),
      headers: headers
    );

    // Handle request response
    final Map<String, dynamic> responseData = json.decode(response.body);
    bool hasError = true;
    String message ='Something went wrong';
    print('register');
    print(responseData);

    //success
    if (response.statusCode == 201) {
      hasError = false;
      message = 'Authentification succeeded';
    }

    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }



  /*
  * Update User Profile
  * update email, password or name
  */
  Future<bool> updateUser({ String email, String name, String password }) async {
    isLoadingData = true;
    notifyListeners();

    // user new data
    final Map<String, dynamic> updateUserData = {
      'email': email != null ? email : authenticatedUser.email,
      'password': password != null ? password : authenticatedUser.password,
      'name': name != null ? name : authenticatedUser.name
    };

    // update user data
    http.Response response = await http.put(
      '${apiUrl}/user/${authenticatedUser.id}',
      body: json.encode(updateUserData),
      headers: headers
    );


    // success
    if(response.statusCode == 200) {

      // update store user
      authenticatedUser = User(
        id: authenticatedUser.id,
        email: email != null ? email : authenticatedUser.email,
        password: password != null ? password : authenticatedUser.password,
        name: name != null ? name : authenticatedUser.name,
        pictureUrl: authenticatedUser.pictureUrl,
        company: authenticatedUser.company,
        vacationRule: authenticatedUser.vacationRule,
        os: authenticatedUser.os,
        pushId: authenticatedUser.pushId,
        token: authenticatedUser.token,
        admin: authenticatedUser.admin,
        superAdmin: authenticatedUser.superAdmin,
        daysOff: authenticatedUser.daysOff,
        summerVacation: authenticatedUser.summerVacation,
        daysOffTaken: authenticatedUser.daysOffTaken,
        summerVacationTaken: authenticatedUser.summerVacationTaken,
        daysOffNoPaidTaken: authenticatedUser.daysOffNoPaidTaken,
        medicalLeaveTaken: authenticatedUser.medicalLeaveTaken,
        nbDaysOffPerYear: authenticatedUser.nbDaysOffPerYear,
        nbSummerVacationPerYear: authenticatedUser.nbSummerVacationPerYear,
        lastSignUp: authenticatedUser.lastSignUp,
        language: allTranslations.currentLanguage,
        autoActionOnChangeYear: authenticatedUser.autoActionOnChangeYear,
        resetOnChangeYear: authenticatedUser.resetOnChangeYear,
        expiryTime: authenticatedUser.expiryTime
      );

      final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
      if(prefs.getDouble('name') != authenticatedUser.name) prefs.setString('name', authenticatedUser.name);
      if(prefs.getDouble('email') != authenticatedUser.email) prefs.setString('email', authenticatedUser.email);
      if(prefs.getDouble('password') != authenticatedUser.password) prefs.setString('password', authenticatedUser.password);

      isLoadingData = false;
      notifyListeners();
      return true;
    } 

    isLoadingData = false;
    notifyListeners();
    return false;
  }




  /*
  * Update selected user
  * 
  */
  Future<bool> updateSelectedUser({
      bool admin,
      bool superAdmin,
      Company company, 
      VacationRule vacationRule,
      double daysOff,
      double summerVacation,
      double daysOffTaken,
      double summerVacationTaken,
      double daysOffNoPaidTaken,
      double medicalLeaveTaken,
      double nbDaysOffPerYear,
      double nbSummerVacationPerYear,
      String language,
      bool autoActionOnChangeYear, 
      bool resetOnChangeYear
  }) async {

    if(admin == null) {
      isLoadingData = true;
      notifyListeners();
    }

    final Map<String, dynamic> requestData = {
      'admin': admin != null ? admin : selectedUser.admin,
      'superAdmin': superAdmin != null ? superAdmin : selectedUser.superAdmin,
      'company': company != null ? company : selectedUser.company,
      'vacationRule': vacationRule != null ? vacationRule : selectedUser.vacationRule,
      'daysOff': daysOff != null ? daysOff : selectedUser.daysOff,
      'summerVacation': summerVacation != null ? summerVacation : selectedUser.summerVacation,
      'daysOffTaken': daysOffTaken != null ? daysOffTaken : selectedUser.daysOffTaken,
      'summerVacationTaken': summerVacationTaken != null ? summerVacationTaken : selectedUser.summerVacationTaken,
      'daysOffNoPaidTaken': daysOffNoPaidTaken != null ? daysOffNoPaidTaken : selectedUser.daysOffNoPaidTaken,
      'medicalLeaveTaken': medicalLeaveTaken != null ? medicalLeaveTaken : selectedUser.medicalLeaveTaken,
      'nbDaysOffPerYear': nbDaysOffPerYear != null ? nbDaysOffPerYear : selectedUser.nbDaysOffPerYear,
      'nbSummerVacationPerYear': nbSummerVacationPerYear != null ? nbSummerVacationPerYear : selectedUser.nbSummerVacationPerYear,
      'language': language != null ? language : selectedUser.language,
      'autoActionOnChangeYear': autoActionOnChangeYear != null ? autoActionOnChangeYear : selectedUser.autoActionOnChangeYear,
      'resetOnChangeYear': resetOnChangeYear != null ? resetOnChangeYear : selectedUser.resetOnChangeYear
    };

    print(requestData);

    // request
    try {
     final http.Response response = await http.put(
        '${apiUrl}/user/${selectedUser.id}',
        headers: headers,
        body: json.encode(requestData)
      );

      // handle request response
      if (response.statusCode == 200) {
        
        if(admin == null) { isLoadingData = false; }

        // create a new request
        final User newUser = User(
          id: selectedUser.id,
          email: selectedUser.email,
          password: selectedUser.password,
          name: selectedUser.name,
          pictureUrl: selectedUser.pictureUrl,
          company: selectedUser.company,
          vacationRule: selectedUser.vacationRule,
          os: selectedUser.os,
          pushId: selectedUser.pushId,
          token: selectedUser.token,
          admin: admin != null ? admin : selectedUser.admin,
          superAdmin: superAdmin != null ? superAdmin : selectedUser.superAdmin,
          daysOff: daysOff != null ? daysOff : selectedUser.daysOff,
          summerVacation: summerVacation != null ? summerVacation : selectedUser.summerVacation,
          summerVacationTaken: summerVacationTaken != null ? summerVacationTaken : selectedUser.summerVacationTaken,
          daysOffNoPaidTaken: daysOffNoPaidTaken != null ? daysOffNoPaidTaken : selectedUser.daysOffNoPaidTaken,
          daysOffTaken: daysOffTaken != null ? daysOffTaken : selectedUser.daysOffTaken,
          medicalLeaveTaken: medicalLeaveTaken != null ? medicalLeaveTaken : selectedUser.medicalLeaveTaken,
          nbDaysOffPerYear: nbDaysOffPerYear != null ? nbDaysOffPerYear : selectedUser.nbDaysOffPerYear,
          nbSummerVacationPerYear: nbSummerVacationPerYear != null ? nbSummerVacationPerYear : selectedUser.nbSummerVacationPerYear,
          lastSignUp: selectedUser.lastSignUp,
          language: language != null ? language : selectedUser.language,
          autoActionOnChangeYear: autoActionOnChangeYear != null ? autoActionOnChangeYear : selectedUser.autoActionOnChangeYear,
          resetOnChangeYear: resetOnChangeYear != null ? resetOnChangeYear : selectedUser.resetOnChangeYear,
          expiryTime: selectedUser.expiryTime
        );

        // replace old request by the new request
        _users[selectedUserIndex] = newUser;

        if(selectedUser.id == authenticatedUser.id) {
          authenticatedUser = newUser;

          final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
          if(prefs.getString('company') != authenticatedUser.company) prefs.setString('company', json.encode(authenticatedUser.company));
          if(prefs.getString('vacationRule') != authenticatedUser.vacationRule) prefs.setString('vacationRule', json.encode(authenticatedUser.vacationRule));
          if(prefs.getBool('admin') != authenticatedUser.admin) prefs.setBool('admin', authenticatedUser.admin);
          if(prefs.getBool('superAdmin') != authenticatedUser.superAdmin) prefs.setBool('superAdmin', authenticatedUser.superAdmin);
          if(prefs.getDouble('daysOff') != authenticatedUser.daysOff) prefs.setDouble('daysOff', authenticatedUser.daysOff);
          if(prefs.getDouble('summerVacation') != authenticatedUser.summerVacation) prefs.setDouble('summerVacation', authenticatedUser.summerVacation);
          if(prefs.getDouble('summerVacationTaken') != authenticatedUser.summerVacationTaken) prefs.setDouble('summerVacationTaken', authenticatedUser.summerVacationTaken);
          if(prefs.getDouble('daysOffNoPaidTaken') != authenticatedUser.daysOffNoPaidTaken) prefs.setDouble('daysOffNoPaidTaken', authenticatedUser.daysOffNoPaidTaken);
          if(prefs.getDouble('daysOffTaken') != authenticatedUser.daysOffTaken) prefs.setDouble('daysOffTaken', authenticatedUser.daysOffTaken);
          if(prefs.getDouble('medicalLeaveTaken') != authenticatedUser.medicalLeaveTaken) prefs.setDouble('medicalLeaveTaken', authenticatedUser.medicalLeaveTaken);
          if(prefs.getDouble('nbDaysOffPerYear') != authenticatedUser.nbDaysOffPerYear) prefs.setDouble('nbDaysOffPerYear', authenticatedUser.nbDaysOffPerYear);
          if(prefs.getDouble('nbSummerVacationPerYear') != authenticatedUser.nbSummerVacationPerYear) prefs.setDouble('nbSummerVacationPerYear', authenticatedUser.nbSummerVacationPerYear);
          if(prefs.getDouble('language') != authenticatedUser.language) prefs.setString('language', authenticatedUser.language);
          if(prefs.getBool('autoActionOnChangeYear') != authenticatedUser.autoActionOnChangeYear) prefs.setBool('autoActionOnChangeYear', authenticatedUser.autoActionOnChangeYear);
          if(prefs.getBool('resetOnChangeYear') != authenticatedUser.resetOnChangeYear) prefs.setBool('resetOnChangeYear', authenticatedUser.resetOnChangeYear);
        }

        notifyListeners();
        return true;
      }
    } catch(error) {
      if(admin == null) { isLoadingData = false; }
      notifyListeners();
      return false;
    }

    return false;
  }


  /*
  * Update admin selected user
  * 
  */
  Future<bool> updateSelectedUserAdmin(bool value) {
    return updateSelectedUser(admin: value);
  }




  /*
  * Update DaysOff selected user
  * 
  */
  Future<bool> updateSelectedUserDaysOff(double value) {
    return updateSelectedUser(daysOff: value);
  }



  /*
  * Update Summer vacation selected user
  * 
  */
  Future<bool> updateSelectedUserSummerVacation(double value) {
    return updateSelectedUser(summerVacation: value);
  }



  /*
  * Update DaysOff selected user
  * 
  */
  Future<bool> updateSelectedUserNbDaysOffPerYear(double value) {
    return updateSelectedUser(nbDaysOffPerYear: value);
  }



  /*
  * Update Summer vacation selected user
  * 
  */
  Future<bool> updateSelectedUserNbSummerVacationPerYear(double value) {
    return updateSelectedUser(nbSummerVacationPerYear: value);
  }



  /*
  * Update autoActionOnChangeYear selected user
  * 
  */
  Future<bool> updateSelectedUserAutoActionOnChangeYear(bool value) {
    return updateSelectedUser(autoActionOnChangeYear: value);
  }




  /*
  * Update resetOnChangeYear selected user
  * 
  */
  Future<bool> updateSelectedUserResetOnChangeYear(bool value) {
    return updateSelectedUser(resetOnChangeYear: value);
  }



  /*
  * Logout
  */
  void logout() async {
    authenticatedUser = null;
    _authTimer.cancel();

    _userSubject.add(false);

    final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
    // prefs.clear(); // clear all the data
    prefs.remove('id');
    prefs.remove('name');
    prefs.remove('email');
    prefs.remove('password');
    prefs.remove('pictureUrl');
    prefs.remove('company');
    prefs.remove('vacationRule');
    prefs.remove('pushId');
    prefs.remove('os');
    prefs.remove('token');
    prefs.remove('admin');
    prefs.remove('superAdmin');
    prefs.remove('daysOff');
    prefs.remove('summerVacation');
    prefs.remove('daysOffTaken');
    prefs.remove('summerVacationTaken');
    prefs.remove('daysOffNoPaidTaken');
    prefs.remove('medicalLeaveTaken');
    prefs.remove('nbDaysOffPerYear');
    prefs.remove('nbSummerVacationPerYear');
    prefs.remove('lastSignUp');
    // prefs.remove('language');
    prefs.remove('autoActionOnChangeYear');
    prefs.remove('resetOnChangeYear');
    prefs.remove('expiryTime');

    selectUser(null);
  }

  // setTime for logout = token expired
  void setAuthTimeOut(int time) {
    _authTimer = Timer(Duration(seconds: time), () {
      logout();
    });
  }




  /*
  * Auto Login
  * check if there is a token in cookies => login + create user
  */
  void autoAuthenticate() async {
    //get cookies
    final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
    final String token = prefs.getString('token');
    final String password = prefs.getString('password');
    final String os = prefs.getString('os');
    final int expiryTimeString = prefs.getInt('expiryTime');

    // check if there is a token
    if (token != null) {
      isLoadingData = true;
      notifyListeners();

      DateTime now = DateTime.now();
      DateTime parsedExpiryTime;
      if(expiryTimeString != null) {
        if(os == 'iOs')
          parsedExpiryTime = DateTime.fromMillisecondsSinceEpoch(expiryTimeString * 1000, isUtc: true);
        else
          parsedExpiryTime = DateTime.fromMillisecondsSinceEpoch(expiryTimeString);
      } else {
        authenticatedUser = null;
        isLoadingData = false;
        notifyListeners();
        return;
      }

      // check if the token is expired or not - if not then autologin
      if(parsedExpiryTime.isBefore(now)) {
        authenticatedUser = null;
        isLoadingData = false;
        notifyListeners();
        return;
      }


      // Cookies
      final String id = prefs.getString('id');
      final String name = prefs.getString('name');
      final String email = prefs.getString('email');
      final String pictureUrl = prefs.getString('pictureUrl');
      final Map<String, dynamic> company = json.decode(prefs.getString('company'));
      final Map<String, dynamic> vacationRule = json.decode(prefs.getString('vacationRule'));
      final String pushId = prefs.getString('pushId');
      final bool admin = prefs.getBool('admin');
      final bool superAdmin = prefs.getBool('superAdmin');
      final double daysOff = prefs.getDouble('daysOff');
      final double summerVacation = prefs.getDouble('summerVacation');
      final double daysOffTaken = prefs.getDouble('daysOffTaken');
      final double summerVacationTaken = prefs.getDouble('summerVacationTaken');
      final double daysOffNoPaidTaken = prefs.getDouble('daysOffNoPaidTaken');
      final double medicalLeaveTaken = prefs.getDouble('medicalLeaveTaken');
      final double nbDaysOffPerYear = prefs.getDouble('nbDaysOffPerYear');
      final double nbSummerVacationPerYear = prefs.getDouble('nbSummerVacationPerYear');
      final DateTime lastSignUp = DateFormat('yyyy/MM/dd', "en_US").parse(prefs.getString('lastSignUp'));
      final int expiryTime = prefs.getInt('expiryTime');
      final String language = prefs.getString('language');
      final bool autoActionOnChangeYear = prefs.getBool('autoActionOnChangeYear');
      final bool resetOnChangeYear = prefs.getBool('resetOnChangeYear');

      // set le language
      if(language != allTranslations.currentLanguage)
        await allTranslations.setNewLanguage(language);

      // set the authentification time
      final tokenLifespan = parsedExpiryTime.difference(now).inSeconds;
      setAuthTimeOut(tokenLifespan);

      headers['Authorization'] = 'Bearer ${token}'; //change headers for autorization

      _userSubject.add(true);

      // Create User
      authenticatedUser = User(
        id: id,
        name: name,
        email: email,
        pictureUrl: pictureUrl,
        company: Company(
          id: company['id'],
          name: company['name'],
          email: company['email'],
          emailBase: company['emailBase'],
          phone: company['phone'],
          language: company['language'],
          address: company['address'],
          country: company['country'],
          vacationRule: company['vacationRule'],
          defaultRule: company['defaultRule']
        ),
        vacationRule: VacationRule(
          id: vacationRule['id'],
          name: vacationRule['name'],
          autoActionOnChangeYear: vacationRule['autoActionOnChangeYear'],
          resetOnChangeYear: vacationRule['resetOnChangeYear'],
          nbDaysOffPerYear: vacationRule['nbDaysOffPerYear'] != null ? vacationRule['nbDaysOffPerYear'].toDouble() : null,
          nbSummerVacationPerYear: vacationRule['nbSummerVacationPerYear'] != null ? vacationRule['nbSummerVacationPerYear'].toDouble() : null,
          resetDate: vacationRule['resetDate'],
          addEachMonth: vacationRule['addEachMonth'] || vacationRule['addEachMonth'] == 'true',
          nbDayAddEachMonth: vacationRule['nbDayAddEachMonth'] != null ? vacationRule['nbDayAddEachMonth'].toDouble() : null,
          summerVacationAddDate: vacationRule['summerVacationAddDate']
        ),
        password: password,
        os: os,
        pushId: pushId,
        token: token,
        admin: admin,
        superAdmin: superAdmin,
        daysOff: daysOff,
        summerVacation: summerVacation,
        summerVacationTaken: summerVacationTaken,
        daysOffNoPaidTaken: daysOffNoPaidTaken,
        daysOffTaken: daysOffTaken,
        medicalLeaveTaken: medicalLeaveTaken,
        nbDaysOffPerYear: nbDaysOffPerYear,
        nbSummerVacationPerYear: nbSummerVacationPerYear,
        lastSignUp: lastSignUp,
        language: language,
        autoActionOnChangeYear: autoActionOnChangeYear,
        resetOnChangeYear: resetOnChangeYear,
        expiryTime: expiryTime
      );

      print('authenticatedUser id');
      print(authenticatedUser.id);

      print('authenticatedUser token');
      print(authenticatedUser.token);


      isLoadingData = false;
      notifyListeners();
    }
  }
}