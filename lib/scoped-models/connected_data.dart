/*
* Connect Data Model
* Connect Data Models
* Methods : 
* - get isLoading, get baseDate, get currentDate
* - changeCurrentDate
*/

import 'dart:convert'; // convert/decode json
import 'dart:async';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http; // http request
import 'package:shared_preferences/shared_preferences.dart'; // cookies
import '../widgets/helpers/global_translation.dart';

import '../models/user.dart';

const bool isProduction = bool.fromEnvironment('dart.vm.product');

class ConnectedDataModel extends Model {
  bool isLoadingData = false;

  User authenticatedUser;

  DateTime currentDateData = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0);

  List<User> listUsers;

  // API
  final String apiUrl = 'https://tranquil-oasis-57899.herokuapp.com';
  
  final Map<String, String> headers = {
    'Accept': 'application/json; text/plain',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
  };


  // filter
  List<String> homePageStatusFilter = ['pending', 'approved', 'rejected'];
  List<String> homePageTypeFilter = ['day off', 'summer vacation', 'medical leave', 'no paid'];
  List<String> adminPageStatusFilter = ['pending', 'approved', 'rejected'];
  List<String> adminPageTypeFilter = ['day off', 'summer vacation', 'medical leave', 'no paid'];
  Map<String, dynamic> homePageDatesFilter = {
    'startDate': null,
    'endDate': null,
    'nbDays': null
  };
  Map<String, dynamic> adminPageDatesFilter = {
    'startDate': null,
    'endDate': null,
    'nbDays': null
  };



  final DateTime today = DateTime.now();
  final int month = DateTime.now().month;
  final double summerVacationInit = 4;


  /************ Http Request *************/


  /*
  * Update UserData
  */
  Future<Map<String, dynamic>> updateUserData ({
      String id,
      String token,
      bool admin,
      double daysOff,
      double summerVacation,
      double summerVacationTaken,
      double daysOffNoPaidTaken,
      double daysOffTaken,
      double medicalLeaveTaken,
      String language,
      bool autoActionOnChangeYear, 
      bool resetOnChangeYear
    }) async {

    final Map<String, dynamic> requestData = {
      'admin': admin != null ? admin : authenticatedUser.admin,
      'daysOff': daysOff != null ? daysOff : authenticatedUser.daysOff,
      'summerVacation': summerVacation != null ? summerVacation : authenticatedUser.summerVacation,
      'summerVacationTaken': summerVacationTaken != null ? summerVacationTaken : authenticatedUser.summerVacationTaken,
      'daysOffNoPaidTaken': daysOffNoPaidTaken != null ? daysOffNoPaidTaken : authenticatedUser.daysOffNoPaidTaken,
      'daysOffTaken': daysOffTaken != null ? daysOffTaken : authenticatedUser.daysOffTaken,
      'medicalLeaveTaken': medicalLeaveTaken != null ? medicalLeaveTaken : authenticatedUser.medicalLeaveTaken,
      'language': language != null ? language : allTranslations.currentLanguage,
      'autoActionOnChangeYear': autoActionOnChangeYear != null ? autoActionOnChangeYear : authenticatedUser.autoActionOnChangeYear,
      'resetOnChangeYear': resetOnChangeYear != null ? resetOnChangeYear : authenticatedUser.resetOnChangeYear
    };

    if(authenticatedUser != null) {
      authenticatedUser = User(
          id: authenticatedUser.id,
          email: authenticatedUser.email,
          password: authenticatedUser.password,
          name: authenticatedUser.name,
          pictureUrl: authenticatedUser.pictureUrl,
          os: authenticatedUser.os,
          pushId: authenticatedUser.pushId,
          token: authenticatedUser.token,
          admin: admin != null ? admin : authenticatedUser.admin,
          daysOff: daysOff != null ? daysOff : authenticatedUser.daysOff,
          summerVacation: summerVacation != null ? summerVacation : authenticatedUser.summerVacation,
          summerVacationTaken: summerVacationTaken != null ? summerVacationTaken : authenticatedUser.summerVacationTaken,
          daysOffNoPaidTaken: daysOffNoPaidTaken != null ? daysOffNoPaidTaken : authenticatedUser.daysOffNoPaidTaken,
          daysOffTaken: daysOffTaken != null ? daysOffTaken : authenticatedUser.daysOffTaken,
          medicalLeaveTaken: medicalLeaveTaken != null ? medicalLeaveTaken : authenticatedUser.medicalLeaveTaken,
          lastSignUp: authenticatedUser.lastSignUp,
          language: language != null ? language : allTranslations.currentLanguage,
          autoActionOnChangeYear: autoActionOnChangeYear != null ? autoActionOnChangeYear : authenticatedUser.autoActionOnChangeYear,
          resetOnChangeYear: resetOnChangeYear != null ? resetOnChangeYear : authenticatedUser.resetOnChangeYear,
          expiryTime: authenticatedUser.expiryTime
        );

        // cookies
        final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
        if(prefs.getBool('admin') != authenticatedUser.daysOff) prefs.setBool('admin', authenticatedUser.admin);
        if(prefs.getDouble('daysOff') != authenticatedUser.daysOff) prefs.setDouble('daysOff', authenticatedUser.daysOff);
        if(prefs.getDouble('summerVacation') != authenticatedUser.summerVacation) prefs.setDouble('summerVacation', authenticatedUser.summerVacation);
        if(prefs.getDouble('summerVacationTaken') != authenticatedUser.summerVacationTaken) prefs.setDouble('summerVacationTaken', authenticatedUser.summerVacationTaken);
        if(prefs.getDouble('daysOffNoPaidTaken') != authenticatedUser.daysOffNoPaidTaken) prefs.setDouble('daysOffNoPaidTaken', authenticatedUser.daysOffNoPaidTaken);
        if(prefs.getDouble('daysOffTaken') != authenticatedUser.daysOffTaken) prefs.setDouble('daysOffTaken', authenticatedUser.daysOffTaken);
        if(prefs.getDouble('medicalLeaveTaken') != authenticatedUser.medicalLeaveTaken) prefs.setDouble('medicalLeaveTaken', authenticatedUser.medicalLeaveTaken);
        if(prefs.getString('language') != authenticatedUser.language) prefs.setString('language', authenticatedUser.language);
        if(prefs.getBool('autoActionOnChangeYear') != authenticatedUser.autoActionOnChangeYear) prefs.setBool('autoActionOnChangeYear', authenticatedUser.autoActionOnChangeYear);
        if(prefs.getBool('resetOnChangeYear') != authenticatedUser.resetOnChangeYear) prefs.setBool('resetOnChangeYear', authenticatedUser.resetOnChangeYear);
    }

    try {
      final http.Response response = await http.put(
        '${apiUrl}/user/${id}',
        headers: headers,
        body: json.encode(requestData)
      );
      if(response != null) {
        return requestData;
      }
    } catch(error) {
      print(error);
      return null;
    }

    return null;
  }
}

class UtilityModel extends ConnectedDataModel {
  /************ Getters *************/
  bool get isLoading {
    return isLoadingData;
  }

  DateTime get currentDate {
    return currentDateData;
  }

  /************ Methods *************/
  void changeCurrentDate(DateTime date) {
    currentDateData = date;
    notifyListeners();
  }
  
}