/*
* Main Model
* Join main models together
* Dependencies : User Model, COnnectedData Model, DayReservation Model, Utility Model
*/
import 'package:scoped_model/scoped_model.dart';

import './user.dart';
import './connected_data.dart';
import './request.dart';
import './filters.dart';

class MainModel extends Model with UserModel, ConnectedDataModel, UtilityModel, RequestModel, FilterModel {}