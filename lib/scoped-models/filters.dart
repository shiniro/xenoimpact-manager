import './connected_data.dart';

class FilterModel extends ConnectedDataModel {
  /******** GETTERS ***********/

  List<String> get homepageStatusFilter {
    return List.from(homePageStatusFilter);
  }

  List<String> get homepageTypeFilter {
    return List.from(homePageTypeFilter);
  }

  List<String> get adminpageStatusFilter {
    return List.from(adminPageStatusFilter);
  }

  List<String> get adminpageTypeFilter {
    return List.from(adminPageTypeFilter);
  }

  Map<String, dynamic> get homePageDates {
    return homePageDatesFilter;
  }

  Map<String, dynamic> get adminPageDates {
    return adminPageDatesFilter;
  }

  /******** ADD ***********/

  void addFilterStatusHomepage(String status) {
    homePageStatusFilter.add(status);
  }

  void addFilterTypeHomepage(String type) {
    homePageTypeFilter.add(type);
  }

  void addFilterStatusAdminpage(String status) {
    adminPageStatusFilter.add(status);
  }

  void addFilterTypeAdminpage(String type) {
    adminPageTypeFilter.add(type);
  }

  /******** REMOVE ***********/

  void removeFilterStatusHomepage(String status) {
    homePageStatusFilter.removeWhere((value) => value == status);
  }

  void removeFilterTypeHomepage(String type) {
    homePageTypeFilter.removeWhere((value) => value == type);
  }

  void removeFilterStatusAdminpage(String status) {
    adminPageStatusFilter.removeWhere((value) => value == status);
  }

  void removeFilterTypeAdminpage(String type) {
    adminPageTypeFilter.removeWhere((value) => value == type);
  }


  /******** CHANGE ***********/

  void changeStartDateHomePageDatesFilter(DateTime value) {
    homePageDatesFilter = {
      'startDate': value,
      'endDate': homePageDatesFilter['endDate'],
      'nbDays': homePageDatesFilter['nbDays'],
    };
  }

  void changeEndDateHomePageDatesFilter(DateTime value) {
    homePageDatesFilter = {
      'startDate': homePageDatesFilter['startDate'],
      'endDate': value,
      'nbDays': homePageDatesFilter['nbDays'],
    };
  }

  void changeNbDaysHomePageDatesFilter(double value) {
    homePageDatesFilter = {
      'startDate': homePageDatesFilter['startDate'],
      'endDate': homePageDatesFilter['endDate'],
      'nbDays': value,
    };
  }


  void changeStartDateAdminPageDatesFilter(DateTime value) {
    adminPageDatesFilter = {
      'startDate': value,
      'endDate': adminPageDatesFilter['endDate'],
      'nbDays': adminPageDatesFilter['nbDays'],
    };
  }

  void changeEndDateAdminPageDatesFilter(DateTime value) {
    adminPageDatesFilter = {
      'startDate': adminPageDatesFilter['startDate'],
      'endDate': value,
      'nbDays': adminPageDatesFilter['nbDays'],
    };
  }

  void changeNbDaysAdminPageDatesFilter(double value) {
    adminPageDatesFilter = {
      'startDate': adminPageDatesFilter['startDate'],
      'endDate': adminPageDatesFilter['endDate'],
      'nbDays': value,
    };
  }



  /******** RESET ***********/

  void resetAllHomeFilters() {
    resetFilterStatusHomepage();
    resetFilterTypeHomepage();
    resetHomePageDatesFilter();
    notifyListeners();
  }


  void resetAllAdminFilters() {
    resetFilterStatusAdminpage();
    resetFilterTypeAdminpage();
    resetAdminPageDatesFilter();
    notifyListeners();
  }

  void resetFilterStatusHomepage() {
    homePageStatusFilter = ['pending', 'approved', 'rejected'];
  }

  void resetFilterTypeHomepage() {
    homePageTypeFilter = ['day off', 'summer vacation', 'medical leave', 'no paid'];
  }

  void resetFilterStatusAdminpage() {
    adminPageStatusFilter = ['pending', 'approved', 'rejected'];
  }

  void resetFilterTypeAdminpage() {
    adminPageTypeFilter = ['day off', 'summer vacation', 'medical leave', 'no paid'];
  }

  void resetHomePageDatesFilter() {
    homePageDatesFilter = {
      'startDate': null,
      'endDate': null,
      'nbDays': null
    };
  }

  void resetAdminPageDatesFilter() {
    adminPageDatesFilter = {
      'startDate': null,
      'endDate': null,
      'nbDays': null
    };
  }
}
