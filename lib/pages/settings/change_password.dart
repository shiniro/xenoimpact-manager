import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

class ChangePasswordPage extends StatefulWidget {
  final MainModel model;

  ChangePasswordPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ChangePasswordPageState();
  }
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  String password = '';

  final GlobalKey<FormState> _userProfilFormKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("settings.change_password"),
      showAppHeader: true,
      appBarLeading: IconButton(icon:Icon(Icons.arrow_back),
        onPressed:() => Navigator.pop(context, false),
      ),
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Form(
            key: _userProfilFormKey,
            child: Column(
              children: <Widget>[
                _buildPasswordTextField(),
                SizedBox(height: 10.0),
                _buildConfirmPasswordTextField(),
                SizedBox(height: 28.0),
                FullWidthButton(
                  text: allTranslations.text('settings.change_password'),
                  onPressed: () => _submitForm(context, widget.model.updateUser),
                )
              ]
            )
          )
        ],
      ),
    );
  }




  /*
  * PASSWORD TEXT FIELD
  */
  Widget _buildPasswordTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        obscureText: true,
        controller: _passwordTextController,
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.password"),
          icon: Icons.lock
        ),
        style: TextStyle(color: Theme.of(context).textTheme.body1.color),
        validator: (String value) {
          if (value.isEmpty) {
            return '비밀번호디가 틀렸습니다. 다시 확인해 주세요.';
          }
          return null;
        },
        onSaved: (String value) {
          password = value;
        },
      )
    );
  }



  /*
  * CONFIRM PASSWORD TEXT FIELD
  */
  Widget _buildConfirmPasswordTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        obscureText: true,
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.confirm_password"),
          icon: Icons.lock
        ),
        style: TextStyle(color: Theme.of(context).textTheme.body1.color),
        validator: (String value) {
          if (value.isEmpty) {
            return 'confirm passord needed';
          } else if(_passwordTextController.text != value) {
            return 'your password is not the same';
          }
          return null;
        },
      )
    );
  }



  /*
  * SUBMIT FORM
  */
  void _submitForm(BuildContext context, Function updateUser) async {
  // if input empty or not correct => show error message and stop function
  if(!_userProfilFormKey.currentState.validate()) {
    return;
  }

  // saveData in variables
  _userProfilFormKey.currentState.save();

    //http request
    bool response = await updateUser(
      password: password
    );
    
    //handle response
    if (response) {
      Navigator.pushReplacementNamed(context, '/');
    } 
  }

}