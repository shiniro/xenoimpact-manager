import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/standard_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

class ModifyProfilPage extends StatefulWidget {
  final MainModel model;

  ModifyProfilPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ModifyProfilPageState();
  }
}

class _ModifyProfilPageState extends State<ModifyProfilPage> {
  List<Map<String, String>> languages = _languageText();
  String selectedLanguage;

  final Map<String, dynamic> _userProfilFormData = {
    'email': null,
    'name': null
  };

  final GlobalKey<FormState> _userProfilFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    setState(() {
      selectedLanguage = allTranslations.currentLanguage;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("settings.modify_profil"),
      showAppHeader: true,
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Column(children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Text('${allTranslations.text("settings.languages")} :'),
            ),
            SizedBox(height: 10.0),
            Container(
              child: Row(
                children: _buildLanguagesList()
              )
            ),
            SizedBox(height: 20.0),
            _buildUserProfilForm(widget.model),
          ],),
        ],
      ),
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/



  List<Widget> _buildLanguagesList() {
    List<Widget> choices = List();

    languages.forEach((item) {
      choices.add(Container(
        padding: EdgeInsets.symmetric(horizontal: 7.0),
        child: ChoiceChip(
          label: Text(item['text']),
          labelStyle: TextStyle(
            color: Colors.white
          ),
          selectedColor: Theme.of(context).accentColor,
          backgroundColor: Color(0xffb0b0b0),
          selected: selectedLanguage == item['language'],
          onSelected: (selected) async{
            if(item['language'] != allTranslations.currentLanguage) {
              await allTranslations.setNewLanguage(item['language']);
              widget.model.updateUserData(
                id: widget.model.userID,
                token: widget.model.user.token,
                language: item['language']
              );
              setState(()  {
                selectedLanguage = item['language'];
                languages = _languageText();
              });
            }
          }
        )
      ));
    });

    return choices;
  }



  /*
  * BUILD LOGIN FORM
  */
  Widget _buildUserProfilForm(MainModel model) {
    return Form(
      key: _userProfilFormKey,
      child: Column(
        children: <Widget>[
          _buildEmailTextField(),
          SizedBox(height: 10.0),
          _buildNameTextField(),
          model.user.password != null ? SizedBox(height: 30.0) : Container(),
          model.user.password != null 
          ? StandardButton(
              text: allTranslations.text("settings.change_password"),
              onPressed: () {
                Navigator.pushNamed(context, '/settings/password');
              }
            )  
          : Container(),
          SizedBox(height: 28.0),
          FullWidthButton(
            text: allTranslations.text('settings.modify_profil'),
            onPressed: () => _submitForm(context, widget.model.updateUser),
          )
        ]
      )
    );
  }



  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('email')),
          SizedBox(height: 10.0),
          Theme(
            data: Theme.of(context).copyWith(
              primaryColor: Colors.white.withOpacity(0.48),
            ),
            child: TextFormField(
              initialValue: widget.model.user.email,
              decoration: buildInputDecoration(
                context: context,
                placeholder: allTranslations.text("authentification.email_placeholder"),
                icon: Icons.email
              ),
              style: TextStyle(color: Theme.of(context).textTheme.body1.color),
              validator: (String value) {
                if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
                  return '가입하신 이메일에서 임시 비밀번호를 확인하세요';
                }
                return null;
              },
              onSaved: (String value) {
                _userProfilFormData['email'] = value;
              },
            )
          )
        ]
      )
    );
  }



  /*
  * BUILD NAME TEXT FIELD
  */
  Widget _buildNameTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('name')),
          SizedBox(height: 10.0),
          Theme(
            data: Theme.of(context).copyWith(
              primaryColor: Colors.white.withOpacity(0.48),
            ),
            child: TextFormField(
              initialValue: widget.model.user.name,
              decoration: buildInputDecoration(
                context: context,
                placeholder: allTranslations.text("name"),
                icon: Icons.person
              ),
              style: TextStyle(color: Theme.of(context).textTheme.body1.color),
              validator: (String value) {
                if (value.isEmpty || value.length < 3) {
                  return 'your name is not long enough';
                }
                return null;
              },
              onSaved: (String value) {
                _userProfilFormData['name'] = value;
              },
            )
          )
        ]
      )
    );
  }


  /******************************/
  /********** METHODS ***********/ 
  /******************************/


  /*
  * SUBMIT FORM
  */
  void _submitForm(BuildContext context, Function updateUser) async {
  // if input empty or not correct => show error message and stop function
  if(!_userProfilFormKey.currentState.validate()) {
    return;
  }

  // saveData in variables
  _userProfilFormKey.currentState.save();

    //http request
    bool response = await updateUser(
      email: _userProfilFormData['email'],
      name: _userProfilFormData['name']
    );
    
    //handle response
    if (response) {
      Navigator.pushReplacementNamed(context, '/');
    } 
  }




  static List<Map<String, String>> _languageText() {
    return [
      {
        'text': allTranslations.text("settings.korean"),
        'language': 'kr'
      }, 
      {
        'text': allTranslations.text("settings.english"),
        'language': 'en'
      },
      {
        'text': allTranslations.text("settings.french"),
        'language': 'fr'
      }
    ];
  }
}