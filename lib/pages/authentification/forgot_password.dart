/*
* Forgot password
* Dependensies: Models - Pages 
* (Homepage, Authentification - FindUser)
* Configuration : theme (font, colors) - route
* AutoAuthentification if there is data in cookie
*/

import 'package:flutter/material.dart';

import '../../scoped-models/main.dart';
import '../../widgets/helpers/global_translation.dart';
import '../../widgets/layout/authentification_layout.dart';
import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/standard_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

class ForgotPasswordPage extends StatefulWidget {
  final MainModel model;

  ForgotPasswordPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ForgotPasswordPageState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  String email;
  final GlobalKey<FormState> _forgotPasswordFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return AuthentificationLayout(
      child: Center(child: Column(children: <Widget>[
        Image.asset(
          'assets/img/logo.png',
          width: 186.0,
          height: 30.0,
        ),
        SizedBox(height: 40.0),
        Text(allTranslations.text("authentification.forgot_password_text")),
        SizedBox(height: 20.0),
        _buildForgotPasswordForm(),
        SizedBox(height: 28.0),
        StandardButton(
          text: allTranslations.text("authentification.back_login"),
          onPressed: () {
            Navigator.pop(context);
          }
        )
        ],),
      )
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * BUILD FORGOT PASSWORD FORM
  */
  Widget _buildForgotPasswordForm() {
    return Form(
      key: _forgotPasswordFormKey,
      child: Column(
        children: <Widget>[
          _buildEmailTextField(),
          SizedBox(height: 28.0),
          FullWidthButton(
            text: allTranslations.text("authentification.reset_password"),
            onPressed: () {},
          )
        ]
      )
    );
  }



  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.email_placeholder"),
          icon: Icons.email,
          placeholderColor: Colors.white.withOpacity(0.50),
          iconColor: Colors.white.withOpacity(.50),
          padding: EdgeInsets.symmetric(horizontal: 20.7, vertical: 14),
          labelColor: Colors.white.withOpacity(0.50),
          enabledBorderColor: Colors.white.withOpacity(0.63)
        ),
        style: TextStyle(color: Colors.white),
        validator: (String value) {
          if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
            return '가입하신 이메일에서 임시 비밀번호를 확인하세요';
          }
          return null;
        },
        onSaved: (String value) {
          email = value;
        },
      )
    );
  }



  /******************************/
  /********** METHODS ***********/ 
  /******************************/


  /*
  * SUBMIT FORM
  */
  void _submitForm(Function sendPasswordResetEmail) async {
    // if input empty or not correct => show error message and stop function
    if(!_forgotPasswordFormKey.currentState.validate()) {
      return;
    }

    // saveData in variables
    _forgotPasswordFormKey.currentState.save();

    //http request
    Map<String, dynamic> response = await sendPasswordResetEmail(email);
    
    //handle response
    if (response['success']) {
      print(response);
    } else {
        //show error in a dialog box
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('An error Occured !'),
            content: Text(response['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
      );
    }
  }

}