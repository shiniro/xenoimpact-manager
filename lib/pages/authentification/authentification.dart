import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart'; // get device info
import 'package:uuid/uuid.dart'; //generate uuid for simulated divice
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../models/auth.dart';

import '../../widgets/layout/authentification_layout.dart';
import '../../widgets/ui_elements/text_separator.dart';
import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/standard_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

class AuthentificationPage extends StatefulWidget {
  final MainModel model;

  AuthentificationPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _AuthentificationPageState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _AuthentificationPageState extends State<AuthentificationPage> {

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  bool _autovalidate = false;
  final Map<String, dynamic> _loginFormData = {
    'email': null,
    'password': null
  };
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();
  AuthMode _authMode = AuthMode.Login;


  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return AuthentificationLayout(
      child: ListView(
        children: <Widget>[
          Center(child: Column(children: <Widget>[
            Image.asset(
              'assets/img/logo.png',
              width: 186.0,
              height: 30.0,
            ),
            SizedBox(height: 40.0),
            _buildLoginForm(),
            _authMode == AuthMode.Login ? _buildLoginWithGoogleButtonBlock(): Container(),
            SizedBox(height: 42.0),
            Center(child: Text(
              allTranslations.text("authentification.new_member"),
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.black.withOpacity(0.79)
              ),
            ),),
            SizedBox(height: 15.0),
            StandardButton(
              text: '${_authMode == AuthMode.Login ? allTranslations.text("authentification.register") : allTranslations.text("authentification.login")}',
              onPressed: () {
                setState(() {
                  _authMode = _authMode == AuthMode.Login ? AuthMode.SignUp : AuthMode.Login;
                });
              }
            )
          ],),),
        ]
      ),   
    );
  }



  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/


  /*
  * BUILD LOGIN FORM
  */
  Widget _buildLoginForm() {
    return Form(
      key: _loginFormKey,
      child: Column(
        children: <Widget>[
          _buildEmailTextField(),
          _authMode == AuthMode.SignUp ? SizedBox(height: 20.0) : Container(),
          _authMode == AuthMode.SignUp ? _buildNameTextField() : Container(),
          SizedBox(height: 20.0),
          _buildPasswordTextField(),
          _authMode == AuthMode.SignUp ? SizedBox(height: 20.0) : Container(),
          _authMode == AuthMode.SignUp ? _buildConfirmPasswordTextField() : Container(),
          _authMode == AuthMode.Login ? SizedBox(height: 11.0) : Container(),
          _authMode == AuthMode.Login ? _buildForgetPassword() : Container(),
          SizedBox(height: 28.0),
          FullWidthButton(
            text: '${_authMode == AuthMode.Login ? allTranslations.text("authentification.login") : allTranslations.text("authentification.register")}',
            onPressed: () => _submitForm(widget.model.signIn, widget.model.register),
          )
        ]
      )
    );
  }



  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.email_placeholder"),
          icon: Icons.email,
          placeholderColor: Colors.white.withOpacity(0.50),
          iconColor: Colors.white.withOpacity(.50),
          padding: EdgeInsets.symmetric(horizontal: 20.7, vertical: 14),
          labelColor: Colors.white.withOpacity(0.50),
          enabledBorderColor: Colors.white.withOpacity(0.63)
        ),
        style: TextStyle(color: Colors.white),
        validator: (String value) {
          if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
            return _autovalidate ? '가입하신 이메일에서 임시 비밀번호를 확인하세요' : '';
          }
          return null;
        },
        onSaved: (String value) {
          _loginFormData['email'] = value;
        },
      )
    );
  }



  /*
  * BUILD NAME TEXT FIELD
  */
  Widget _buildNameTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("name"),
          icon: Icons.person,
          placeholderColor: Colors.white.withOpacity(0.50),
          iconColor: Colors.white.withOpacity(.50),
          padding: EdgeInsets.symmetric(horizontal: 20.7, vertical: 14),
          labelColor: Colors.white.withOpacity(0.50),
          enabledBorderColor: Colors.white.withOpacity(0.63)
        ),
        style: TextStyle(color: Colors.white),
        validator: (String value) {
          if (value.isEmpty || value.length < 3) {
            return _autovalidate ? 'your name is not long enough' : '';
          }
          return null;
        },
        onSaved: (String value) {
          _loginFormData['name'] = value;
        },
      )
    );
  }




  /*
  * PASSWORD TEXT FIELD
  */
  Widget _buildPasswordTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        obscureText: true,
        controller: _passwordTextController,
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.password"),
          icon: Icons.lock,
          placeholderColor: Colors.white.withOpacity(0.50),
          iconColor: Colors.white.withOpacity(.50),
          padding: EdgeInsets.symmetric(horizontal: 20.7, vertical: 14),
          labelColor: Colors.white.withOpacity(0.50),
          enabledBorderColor: Colors.white.withOpacity(0.63)
        ),
        style: TextStyle(color: Colors.white),
        validator: (String value) {
          if (value.isEmpty) {
            return '비밀번호디가 틀렸습니다. 다시 확인해 주세요.';
          }
          return null;
        },
        onSaved: (String value) {
          _loginFormData['password'] = value;
        },
      )
    );
  }



  /*
  * CONFIRM PASSWORD TEXT FIELD
  */
  Widget _buildConfirmPasswordTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.white.withOpacity(0.48),
      ),
      child: TextFormField(
        obscureText: true,
        decoration: buildInputDecoration(
          context: context,
          placeholder: allTranslations.text("authentification.confirm_password"),
          icon: Icons.lock,
          placeholderColor: Colors.white.withOpacity(0.50),
          iconColor: Colors.white.withOpacity(.50),
          padding: EdgeInsets.symmetric(horizontal: 20.7, vertical: 14),
          labelColor: Colors.white.withOpacity(0.50),
          enabledBorderColor: Colors.white.withOpacity(0.63)
        ),
        style: TextStyle(color: Colors.white),
        validator: (String value) {
          if (value.isEmpty) {
            return 'confirm passord needed';
          } else if(_passwordTextController.text != value) {
            return 'your password is not the same';
          }
          return null;
        }
      )
    );
  }



  /*
  * FORGET PASSWORD LINK
  */
  Widget _buildForgetPassword() {
    return Align(
      alignment: Alignment.centerRight,
      child: InkWell(
        child: Text(
          allTranslations.text("authentification.forgot_password"),
          style: TextStyle(
            fontSize: 12.0,
            color: Colors.white.withOpacity(0.50)
          )
        ),
        onTap: () => Navigator.pushNamed(context, '/forgot-password'),
      ),
    );
  }


  
  /*
  * LOGIN WITH GOOGLE BUTTON BLOCK
  */
  Widget _buildLoginWithGoogleButtonBlock() {
    return Container(
      child: Column(children: <Widget>[
        SizedBox(height: 25.0),
        TextSeparator(
          separatorColor: Colors.white.withOpacity(0.59),
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          text: Text(
            allTranslations.text("authentification.or_connect_with"),
            style: TextStyle(
              color: Colors.white.withOpacity(0.59),
            )
          ),
        ),
        SizedBox(height: 25.0),
        _buildLoginWithGoogleButton(),
      ],)
    );
  }




  /*
  * LOGIN WITH GOOGLE BUTTON
  */
  Widget _buildLoginWithGoogleButton() {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        padding: EdgeInsets.symmetric(vertical: 14.0, horizontal: 20.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        color: Color(0xffdc4337),
        textColor: Colors.white,
        child: Container(
          height: 25.0,
          child: Row(children: <Widget>[
            Image.asset(
              'assets/img/icon-google-plus.png',
              width: 25.0,
              height: 27.0,
            ),
            SizedBox(width: 15.0),
            VerticalDivider(
              color: Colors.white,
              width: 1.0,
            ),
            SizedBox(width: 20.0),
            Text(
              allTranslations.text("authentification.connect_with_google"),
              style: TextStyle(
                fontSize: 14.0,
                color: Colors.white
              )
            ),
          ],)
        ),
        onPressed: () async {
          final String pushId = await _getId();
          final String os = await _getOs();
          final Map<String, dynamic> userInformation = await widget.model.signInWithGoogle(pushId, os);

          if(userInformation['success']) {
            Navigator.pushReplacementNamed(context, '/');
          }
        }
      )
    );
  }



  /******************************/
  /********** METHODS ***********/ 
  /******************************/


  /*
  * GET PUSHID
  */
  Future<String> _getId() async {
    //ios
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      // simulator -> geenrate a id
      if(!iosDeviceInfo.isPhysicalDevice) {
        return Uuid().v4();
      }
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else { // android
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      // simulator -> geenrate a id
      if(androidDeviceInfo.isPhysicalDevice) {
        return Uuid().v4();
      }
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  /*
  * GET PUSHID
  */
  Future<String> _getOs() async {
    //ios
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return 'iOs'; // unique ID on iOS
    } else { // android
      return 'Android'; // unique ID on Android
    }
  }


  /*
  * SUBMIT FORM
  */
  void _submitForm(Function signIn, Function register) async {
    // if input empty or not correct => show error message and stop function
    if(!_loginFormKey.currentState.validate()) {
      return;
    }

    // saveData in variables
    _loginFormKey.currentState.save();

    final String pushId = await _getId();
    final String os = await _getOs();

    if (pushId.length > 0) {
      //http request
      Map<String, dynamic> userInformation;
      if(_authMode == AuthMode.Login) {
        userInformation = await signIn(_loginFormData['email'], _loginFormData['password'], pushId, os);
      } else {
        userInformation = await register(_loginFormData['email'], _loginFormData['password'], _loginFormData['name'], pushId, os);
      }
      
      //handle response
      if (userInformation['success']) {
        print(userInformation);
        if(_authMode == AuthMode.SignUp) {
          setState(() {
            _authMode = AuthMode.Login;
          });
        }
        Navigator.pushReplacementNamed(context, '/');
      } else {
         //show error in a dialog box
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('An error Occured !'),
              content: Text(userInformation['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }
    }
  }
}