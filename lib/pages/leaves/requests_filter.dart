import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; // for DateFormat
import 'package:scoped_model/scoped_model.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../widgets/layout/global_layout.dart';

import '../../widgets/ui_elements/title_block.dart';
import '../../widgets/ui_elements/multi_select.dart';

import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

GlobalKey<MultiSelectState> globalStatusKey = GlobalKey();
GlobalKey<MultiSelectState> globalTypeKey = GlobalKey();

class RequestsFilterPage extends StatefulWidget {
  final MainModel model;
  final String page;

  RequestsFilterPage(this.model, this.page);

  @override
  State<StatefulWidget> createState() {
    return _RequestFilterPageState();
  }
}

class _RequestFilterPageState extends State<RequestsFilterPage> {
  final List<String> _status = ['pending', 'approved', 'rejected'];
  final List<String> _types = ['day off', 'summer vacation', 'medical leave', 'no paid'];

  TextEditingController _startDateTextController = TextEditingController();
  TextEditingController _endDateTextController = TextEditingController();
  TextEditingController _nbDaysTextController = TextEditingController();

  final DateTime today = new DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0, 0, 0);

  final format = DateFormat("yyyy/MM/dd");

  @override
  void initState() {
    if((widget.model.homePageDates['startDate'] != null && widget.page == 'home') || (widget.model.adminPageDates['startDate'] != null && widget.page == 'admin')) {
      _startDateTextController = TextEditingController(text : DateFormat("yyyy/MM/dd").format(widget.page == 'home' ? widget.model.homePageDates['startDate'] : widget.model.adminPageDates['startDate']));
    }
    if((widget.model.homePageDates['endDate'] != null && widget.page == 'home') || (widget.model.adminPageDates['endDate'] != null && widget.page == 'admin')) {
      _endDateTextController = TextEditingController(text : DateFormat("yyyy/MM/dd").format(widget.page == 'home' ? widget.model.homePageDates['endDate'] : widget.model.adminPageDates['endDate']));
    }
    if((widget.model.homePageDates['nbDays'] != null && widget.page == 'home') || (widget.model.adminPageDates['nbDays'] != null && widget.page == 'admin')) {
      _nbDaysTextController = TextEditingController(text : widget.page == 'home' ? widget.model.homePageDates['nbDays'].toString() : widget.model.adminPageDates['nbDays'].toString());
    }
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("filters"),
      appBarLeading: IconButton(icon:Icon(Icons.arrow_back),
        onPressed:() => Navigator.pop(context, false),
      ),
      showAppHeader: true,
      body: ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
        List<String> _statusFilter = widget.page == 'home' ? model.homepageStatusFilter : model.adminpageStatusFilter;
        List<String> _typeFilter  = widget.page == 'home' ? model.homepageTypeFilter : model.adminpageTypeFilter;


        return  ListView(
          children: <Widget>[
            Center(
              child: Container(
                child: Column(children: <Widget>[
                  TitleBlock(title: allTranslations.text("status")),
                  _buildListTags(_statusFilter, _status, 'status', widget.page),
                  TitleBlock(title: allTranslations.text("types")),
                  _buildListTags(_typeFilter, _types, 'type', widget.page),
                  TitleBlock(title: allTranslations.text('request_list.Date')),
                  _buildDateFilters(context),
                  Container(
                    padding: EdgeInsets.all(20.0),
                    child: FullWidthButton(
                      text: allTranslations.text('reset_filters'),
                      onPressed: () => _resetFilters(),
                    ),
                  )
                ],)
              )
            )
          ]
        );
      })
    );
  }



  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * LIST TAGS
  */
  Widget _buildListTags(List<String> filter, List<String> list, String type, String page) {
    return Container(
      padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0, bottom: 20.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: MultiSelect(
          key: type == 'status' ? globalStatusKey : globalTypeKey,
          list: list,
          filter: filter,
          type: type,
          page: page,
          onSelectionChanged: (selectedList) {}
        )
      )
    );
  }


  /*
  * BUILD DATE FILTERS
  */
  Widget _buildDateFilters(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(children: <Widget>[
        _buildStartDateTextField(context),
        SizedBox(height: 20.0),
        _buildEndDateTextField(context),
        SizedBox(height: 20.0),
        _buildNbDays(context),
      ],),
    );
  }


  /*
  * START DATE DATETIME FIELD
  */
  Widget _buildStartDateTextField(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('request_form.start_date')),
          SizedBox(height: 10.0),
          DateTimeField(
            controller: _startDateTextController,
            decoration: buildInputDecoration(
              context: context,
              placeholder: 'YYYY/MM/DD',
              padding: EdgeInsets.symmetric(horizontal: 10.0)
            ),
            initialValue: widget.page == 'home' ? widget.model.homePageDates['startDate'] : widget.model.adminPageDates['startDate'],
            format: format,
            onChanged: (DateTime value) {
              widget.page == 'home' ? widget.model.changeStartDateHomePageDatesFilter(value) :  widget.model.changeStartDateAdminPageDatesFilter(value);
            },
            validator: (value) {
              if (value == null) {
                return 'Is required';
              } else if(value.isBefore(today)) {
                return 'Should be in the future';
              } else if (value.isAfter(DateFormat('yy/MM/dd').parse(_endDateTextController.text))) {
                return 'Should be after the end date';
              }
              return null;
            },
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime.now(),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100));
            },
          ),
        ],
      )
    );
  }



  /*
  * END DATE DATETIME FIELD
  */
  Widget _buildEndDateTextField(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('request_form.end_date')),
          SizedBox(height: 10.0),
          DateTimeField(
            controller: _endDateTextController,
            decoration: buildInputDecoration(
              context: context,
              placeholder: 'YYYY/MM/DD',
              padding: EdgeInsets.symmetric(horizontal: 10.0)
            ),
            format: format,
            initialValue: widget.page == 'home' ? widget.model.homePageDates['endDate'] : widget.model.adminPageDates['endDate'],
            onChanged: (DateTime value) {
              widget.page == 'home' ? widget.model.changeEndDateHomePageDatesFilter(value) :  widget.model.changeEndDateAdminPageDatesFilter(value);
            },
            validator: (value) {
              print(value);
              if (value == null) {
                return 'Is required';
              } else if(value.isBefore(today)) {
                return 'Should be in the future';
              } else if (value.isBefore(DateFormat('yy/MM/dd').parse(_startDateTextController.text))) {
                return 'Should be after the end date';
              }
              return null;
            },
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime.now(),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100));
            },
          ),
        ],
      )
    );
  }


  /*
  * NUMBER OF DAYS TEXTFIELD
  */
  Widget _buildNbDays(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('number_of_days')),
          SizedBox(height: 10.0),
          TextField(
            keyboardType: TextInputType.number,
            controller: _nbDaysTextController,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: '0.5',
              icon: Icons.timer
            ),
            onChanged: (String value) {
              widget.page == 'home' ? widget.model.changeNbDaysHomePageDatesFilter(double.parse(value)) :  widget.model.changeNbDaysAdminPageDatesFilter(double.parse(value));
            },
          )
        ],
      )
    );
  }



  void _resetFilters() {
    widget.page == 'home' ? widget.model.resetAllHomeFilters() : widget.model.resetAllAdminFilters();
    setState(() {
      _startDateTextController = TextEditingController(text : '');
      _endDateTextController = TextEditingController(text : '');
      _nbDaysTextController = TextEditingController(text : '');
    });
    globalStatusKey.currentState.resetFilter();
    globalTypeKey.currentState.resetFilter();
  }
}