import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../models/request.dart';
import '../../scoped-models/main.dart';
import './request_leave.dart';
import '../../widgets/layout/global_layout.dart';

class ModifyRequestPage extends StatelessWidget {
  final MainModel model;
  final Request request;

  ModifyRequestPage(this.model, this.request);

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text('request_form.modify_status'),
      showAppHeader: true,
      appBarLeading: IconButton(icon:Icon(Icons.arrow_back),
        onPressed:() => Navigator.pop(context, false),
      ),
      body: RequestLeavePage(model, request)
    );
  }
}