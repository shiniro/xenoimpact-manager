import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:intl/intl.dart'; // for DateFormat
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../models/request.dart';
import '../../widgets/ui_elements/title_block.dart';
import '../../widgets/ui_elements/tag.dart';
import '../../widgets/ui_elements/tile_text.dart';

class RequestStatusPage extends StatefulWidget {
  final MainModel model;
  final bool adminMode;

  RequestStatusPage(this.model, this.adminMode);

  @override
  State<StatefulWidget> createState() {
    return _RequestStatusPageState();
  }
}

class _RequestStatusPageState extends State<RequestStatusPage> {

  final DateTime today = DateTime.now();

  final List<Map<String, dynamic>> titleBlock = [
    {
      'status': 'pending',
      'type': null,
      'title': allTranslations.text("request_list.pending"),
      'icon': Icons.access_time
    },
    {
      'status': null,
      'type': 'day off',
      'title': allTranslations.text("request_list.days_off"),
      'icon': Icons.wb_sunny
    },
    {
      'status': null,
      'type': 'medical leave',
      'title': allTranslations.text("request_list.medical_leaves"),
      'icon': Icons.healing
    },
    {
      'status': null,
      'type': 'summer vacation',
      'title': allTranslations.text("request_list.summer_vacation"),
      'icon': Icons.beach_access
    },
    {
      'status': null,
      'type': 'no paid',
      'title': allTranslations.text("request_list.no_paid"),
      'icon': Icons.beach_access
    }
  ];

  @override
  void initState() {
    widget.model.fetchRequests(widget.model.authenticatedUser.company.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
      Widget content = Center(child: Text(allTranslations.text('request_list.no_request_found')));
      if(model.isLoading) {
        content = Center(child: CircularProgressIndicator());
      } else {
        content = Container(
          child: ListView(children: <Widget>[
            !widget.adminMode 
              ? Container(
                child: _buildLeavesCountBlock(model),
              ) 
              : Container(),
            Container(
              child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: titleBlock.length,
                itemBuilder: (BuildContext context, int index) {
                  List requests;
                  if(widget.adminMode) requests = model.allRequestByStatusAndType(
                    model.adminPageStatusFilter.contains(titleBlock[index]['status']) ? titleBlock[index]['status'] : null,
                    model.adminPageTypeFilter.contains(titleBlock[index]['type']) ? titleBlock[index]['type'] : null,
                    model.adminPageDates['startDate'] != null ? model.adminPageDates['startDate'] : null,
                    model.adminPageDates['endDate'] != null ? model.adminPageDates['endDate'] : null,
                    model.adminPageDates['nbDays'] != null ? model.adminPageDates['nbDays'] : null,
                    'admin'
                  );
                  else requests = model.allRequestByStatusAndTypeAndUserId(
                    model.homePageStatusFilter.contains(titleBlock[index]['status']) ? titleBlock[index]['status'] : null,
                    model.homePageTypeFilter.contains(titleBlock[index]['type']) ? titleBlock[index]['type'] : null,
                    model.homePageDates['startDate'] != null ? model.homePageDates['startDate'] : null,
                    model.homePageDates['endDate'] != null ? model.homePageDates['endDate'] : null,
                    model.homePageDates['nbDays'] != null ? model.homePageDates['nbDays'] : null,
                    'home'
                  );
                  String title = titleBlock[index]['title'];
                  IconData icon = titleBlock[index]['icon'];
                  return requests.length > 0 ? _buildRequestsList(model, requests, title, icon) : Container();
                }
              )
            )
          ],)
        );
      }
      return RefreshIndicator(
        child: content, 
        onRefresh: () => model.fetchRequests(),
      );
    },);
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/


  /*
  * COUNT LEAVES BLOCK
  */
  Widget _buildLeavesCountBlock(MainModel model) {
    List<Map<String, dynamic>> leaves = [
      {
        'title': 'days off left',
        'count': model.user.daysOff,
        'color': Color(0xff64B6AC),
        'icon': Icons.wb_sunny,
        'textColor': Colors.white
      },
      {
        'title': 'summer vacation left',
        'count': model.user.summerVacation,
        'color': Color(0xffF4F1BB),
        'icon': Icons.beach_access,
        'textColor': Theme.of(context).textTheme.body1.color
      },
      {
        'title': 'days off taken',
        'count': model.user.daysOffTaken,
        'color': Color(0xff9BC1BC),
        'icon': Icons.calendar_today,
        'textColor': Colors.white
      },
      {
        'title': 'medical leave taken',
        'count': model.user.medicalLeaveTaken,
        'color': Color(0xffD6EFFF),
        'icon': Icons.healing,
        'textColor': Theme.of(context).textTheme.body1.color
      },
    ];


    return GridView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: leaves.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: (MediaQuery.of(context).size.width / (MediaQuery.of(context).size.height / 2.4))
      ),
      itemBuilder: (BuildContext context, int index) {
        return _buildLeaveCount(leaves[index]['title'], leaves[index]['count'], leaves[index]['icon'], leaves[index]['color'], leaves[index]['textColor']);
      },
    );
  }



  /*
  * COUNT LEAVE
  */
  Widget _buildLeaveCount(String title, double count, IconData icon, Color color, Color textColor) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 15.0,
        vertical: 20.0
      ),
      decoration: BoxDecoration(
        color: color,
      ),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
            Icon(
              icon,
              color: textColor,
              size: 40.0
            ),
            SizedBox(height: 10.0),
            Text(
              count.toString(),
              style: TextStyle(
                color: textColor,
                fontSize: 18.0,
                fontWeight: FontWeight.bold
              ),
            ),
            Text(
              title,
              style: TextStyle(color: textColor),
            )
          ],
        )
      )
    );
  }



  /*
  * REQUESTS LEAVE LIST
  */
  Widget _buildRequestsList(MainModel model, List requests, String title, IconData icon) {

    return Container(
      child: Column(children: <Widget>[
        TitleBlock(
          title: title,
          icon: icon
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: requests.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              key: Key(requests[index].id),
              direction: widget.adminMode ? DismissDirection.horizontal : (requests[index].startDate.isAfter(today) ? DismissDirection.endToStart : null),
              confirmDismiss: (DismissDirection direction) async {
                if(!widget.adminMode) {
                  if(direction == DismissDirection.endToStart) {
                    model.selectRequest(requests[index].id);
                    model.deleteRequestLeave();
                  }
                  return true;
                } else {
                  model.selectRequest(requests[index].id);
                  if(requests[index].status == 'pending') {
                    if(direction == DismissDirection.startToEnd) {
                      model.updateRequestStatus('approved');
                    } else if(direction == DismissDirection.endToStart) {
                      model.updateRequestStatus('rejected');
                    }
                  }
                  if (requests[index].status == 'approved') {
                    if(direction == DismissDirection.startToEnd) {
                      model.updateRequestStatus('pending');
                    } else if(direction == DismissDirection.endToStart) {
                      model.updateRequestStatus('rejected');
                    }
                  } else if (requests[index].status == 'rejected') {
                    if(direction == DismissDirection.startToEnd) {
                      model.updateRequestStatus('approved');
                    } else if(direction == DismissDirection.endToStart) {
                      model.updateRequestStatus('pending');
                    } 
                  }
                  return false;
                }
              },
              background: widget.adminMode
                ? requests[index].status == 'pending' || requests[index].status == 'rejected' ?
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      color: Color(0xff07ad7c),
                      alignment: Alignment.centerLeft,
                      child: Icon(Icons.check)
                    )
                  : Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      color: Color(0xfff18f1b),
                      alignment: Alignment.centerLeft,
                      child: Icon(Icons.access_time)
                    )
                : Container(),
              secondaryBackground: widget.adminMode
                ? requests[index].status == 'pending' || requests[index].status == 'approved' ?
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      color: Color(0xffff5a4d),
                      alignment: Alignment.centerRight,
                      child: Icon(Icons.block)
                    ) 
                  : Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      color: Color(0xfff18f1b),
                      alignment: Alignment.centerRight,
                      child: Icon(Icons.access_time)
                    ) 
                : Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  color: Color(0xffff5a4d),
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.delete)
                ),
              child: Column(
                children: <Widget>[
                  _buildTiles(requests[index])
                ],
              ),
            );
          },
          )
      ],)
    );
  }



  /*
  * BUILD LIST OF TILES
  */
  Widget _buildTiles(Request request) {
    Color tagColor;

    switch(request.status) {
      case 'pending':
        tagColor = Color(0xfff18f1b);
        break;
      case 'approved':
        tagColor = Color(0xff07ad7c);
        break;
      case 'rejected':
        tagColor = Color(0xffdc4337);
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Color(0xffedf1f2)),
        ),
      ),
      child: ListTile(
        onTap: () {
          if(!widget.adminMode) {
            if(request.status == 'pending' && request.startDate.isAfter(today)) {
              Navigator.pushNamed<dynamic>(context, '/request/' + request.id);
            }
          } else {
            Navigator.pushNamed<dynamic>(context, '/request/status/' + request.id);
          }
        },
        title: TileText(label: allTranslations.text('request_list.Date'), value: '${DateFormat("yyyy-MM-dd").format(request.startDate)} to ${DateFormat("yyyy-MM-dd").format(request.endDate)}'),
        subtitle: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 5.0),
              TileText(label: allTranslations.text('number_of_days'), value: request.nbDays.toString()),
              SizedBox(height: 5.0),
              TileText(label: allTranslations.text('type'), value: request.type),
              widget.adminMode ? SizedBox(height: 5.0) : Container(),
              widget.adminMode ? TileText(label: allTranslations.text('name'), value: request.userName.toString()) : Container(),
              request.comment != null ? SizedBox(height: 5.0) : Container(),
              request.comment != null ? TileText(label: allTranslations.text('comment'), value: request.comment) : Container(),
            ],
          )
        ),
        trailing: Tag(
          text: request.status,
          color: tagColor
        )
      )
    );
  }
}