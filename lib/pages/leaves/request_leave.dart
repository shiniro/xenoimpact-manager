import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; // for DateFormat
import 'package:scoped_model/scoped_model.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';

import '../../models/request.dart';

import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';
import '../../widgets/ui_elements/image_input.dart';
import '../../widgets/ui_elements/block_text.dart';

class RequestLeavePage extends StatefulWidget {
  final MainModel model;
  final Request request;

  RequestLeavePage(this.model, [this.request]);

  @override
  State<StatefulWidget> createState() {
    return _RequestLeavePageState();
  }
}

class _RequestLeavePageState extends State<RequestLeavePage> {

  final Map<String, dynamic> _requestLeaveFormData = {
    'startDate': null,
    'endDate': null,
    'nbDays': null,
    'type': null,
    'image': null
  };

  final DateTime today = new DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0, 0, 0);

  final format = DateFormat("yyyy/MM/dd");

  final GlobalKey<FormState> _requestLeaveFormKey = GlobalKey<FormState>();
  TextEditingController _startDateTextController = TextEditingController();
  TextEditingController _endDateTextController = TextEditingController();

  List<String> _leaveType = ['day off', 'summer vacation', 'medical leave', 'no paid'];
  String _currentTypeSelected = 'day off';

  /********* Init State **********/
  @override
  void initState() {
    if(widget.request != null) {
      setState(() {
        _currentTypeSelected = widget.request.type;
      });
      if(widget.request != null && widget.request.startDate != null) {
        _startDateTextController = TextEditingController(text : DateFormat("yyyy/MM/dd").format(widget.request.startDate));
      }
      if(widget.request != null && widget.request.endDate != null) {
        _endDateTextController = TextEditingController(text : DateFormat("yyyy/MM/dd").format(widget.request.endDate));
      }
    }
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
      Widget _content = Center(child: Text(allTranslations.text('request_list.no_request_found')));
      if(model.isLoading) {
        _content = Center(child: CircularProgressIndicator());
      } else {
        _content = ListView(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(children: <Widget>[
                  model.user.daysOff != null && model.user.summerVacation != null 
                    ? _buildLeavesCountBlock(context, model)
                    : Container(),
                  model.user.daysOff != null && model.user.summerVacation != null 
                    ? SizedBox(height: 20.0,)
                    : Container(),
                  _buildRequestForm(context)
                ],
              ),
              )
            )
          ]
        );
      }
      return _content;
    }
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * REQUEST LEAVE FORM
  */
  Widget _buildRequestForm(BuildContext context) {
    return Form(
      key: _requestLeaveFormKey,
      child: Column(
        children: <Widget>[
          _buildStartDateTextField(context),
          SizedBox(height: 20.0,),
          _buildEndDateTextField(context),
          // _buildStartEndDate(context),
          SizedBox(height: 20.0,),
          _buildNbDays(context),
          SizedBox(height: 20.0,),
          _buildDropdownType(context),
          SizedBox(height: 20.0,),
          ImageInput(setImage: _setImage, request: widget.request),
          SizedBox(height: 40.0,),
          FullWidthButton(
            text: widget.request != null ? allTranslations.text('request_form.modify_status') : allTranslations.text('request_form.request_leave'),
            onPressed: () => _submitForm(context, widget.model.addRequestLeave, widget.model.updateRequestLeave, widget.model.selectRequest),
          )
        ]
      )
    );
  }




  /*
  * LEAVES COUNTS BLOCK
  */
  Widget _buildLeavesCountBlock(BuildContext context, MainModel model) {
    return BlockText(
      child: RichText(
        text: TextSpan(
          style: TextStyle(color: Theme.of(context).textTheme.body1.color),
          children: <TextSpan>[
            TextSpan(
              text: '${allTranslations.text('request_form.you_have')} ',
            ),
            TextSpan(
              text: model.user.daysOff.toString(),
              style: TextStyle(color: Theme.of(context).accentColor)
            ),
            TextSpan(
              text: ' ${allTranslations.text('request_form.days_off_left_and')} '
            ),
            TextSpan(
              text: model.user.summerVacation.toString(),
              style: TextStyle(color: Theme.of(context).accentColor)
            ),
            TextSpan(
              text: ' ${allTranslations.text('request_form.summer_vacation_days_left')}.'
            ),
          ]
        )
      )
    );
  }




  /*
  * START DATE TEXTFIELD
  */
  Widget _buildStartDateTextField(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('request_form.start_date')),
          SizedBox(height: 10.0),
          DateTimeField(
            controller: _startDateTextController,
            decoration: buildInputDecoration(
              context: context,
              placeholder: 'YYYY/MM/DD',
              padding: EdgeInsets.symmetric(horizontal: 10.0)
            ),
            format: format,
            onSaved: (value) {
              _requestLeaveFormData['startDate'] = value;
            },
            validator: (value) {
              if (value == null) {
                return 'Is required';
              } else if (value.isAfter(DateFormat('yy/MM/dd').parse(_endDateTextController.text))) {
                return 'Should be after the end date';
              }
              return null;
            },
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(2000),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100));
            },
          ),
        ],
      )
    );
  }



  /*
  * END DATE TEXTFIELD
  */
  Widget _buildEndDateTextField(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('request_form.end_date')),
          SizedBox(height: 10.0),
          DateTimeField(
            controller: _endDateTextController,
            decoration: buildInputDecoration(
              context: context,
              placeholder: 'YYYY/MM/DD',
              padding: EdgeInsets.symmetric(horizontal: 10.0)
            ),
            format: format,
            onSaved: (value) {
              _requestLeaveFormData['endDate'] = value;
            },
            validator: (value) {
              print(value);
              if (value == null) {
                return 'Is required';
              } else if (value.isBefore(DateFormat('yy/MM/dd').parse(_startDateTextController.text))) {
                return 'Should be after the end date';
              }
              return null;
            },
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(2000),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100));
            },
          ),
        ],
      )
    );
  }



  /*
  * ROW START AND END DATE
  */
  Widget _buildStartEndDate(BuildContext context) {
    return Container(
      child: Row(children: <Widget>[
        Expanded(
          child: _buildStartDateTextField(context),
        ),
        Container(
          width: 50.0,
          padding: EdgeInsets.only(top: 30.0, right: 20.0, left: 20.0),
          child: Center(
            child: Divider(color: Color(0xffff8290ad)),
          )
        ),
         Expanded(
          child:  _buildEndDateTextField(context),
         ),
      ],)
    );
  }



  /*
  * NUMBER OF DAYS TEXTFIELD
  */
  Widget _buildNbDays(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('number_of_days')),
          SizedBox(height: 10.0),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: '0.5',
              icon: Icons.timer
            ),
            initialValue: widget.request != null && widget.request.nbDays != null ? widget.request.nbDays.toString() : '',
            validator: (String value) {
              /* String _testEndValue = _endDateTextController.text;
              String _testStartDate = _startDateTextController.text;
              if(!_testEndValue.contains('/')) _testEndValue = _testEndValue.substring(0, 4) + '/' + _testEndValue.substring(4, 6) + '/' + _testEndValue.substring(6, _testEndValue.length);
              if(!_testStartDate.contains('/')) _testStartDate = _testStartDate.substring(0, 4) + '/' + _testStartDate.substring(4, 6) + '/' + _testStartDate.substring(6, _testStartDate.length);
              final int difference = DateFormat('yy/MM/dd').parse(_testEndValue).difference(DateFormat('yy/MM/dd').parse(_testStartDate)).inDays;
              final double days = double.parse(value); */
              if (value.isEmpty) {
                return 'The number of day is required';
              } /* else if((days%1 == 0 && difference != days) || (days%1 != 0 && days < difference - 1) || (days%1 != 0 && days > difference + 1)) {
                return "The days and the date doesn't match";
              } */
              return null;
            },
            onSaved: (String value) {
              _requestLeaveFormData['nbDays'] = value;
            },
          )
        ],
      )
    );
  }



  /*
  * TYPE DROPDOWN
  */
  Widget _buildDropdownType(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('type')),
          SizedBox(height: 10.0),
          DropdownButtonFormField<String>(
            decoration: buildInputDecoration(
              context: context, 
              placeholder: '',
              icon: Icons.local_offer
            ),
            items: _leaveType.map((String type){
              return DropdownMenuItem<String>(
                value: type,
                child: Text(allTranslations.text(type.replaceAll(new RegExp(r' '), '_'))),
              );
            }).toList(),
            value: _currentTypeSelected,
            onChanged: (String value) {
              setState((){
                _currentTypeSelected = value;
              });
            },
            validator: (String value) {
              if(widget.model.user.daysOff == 0 && value == 'day off') {
                return "You don't have enought day off left";
              } else if(widget.model.user.summerVacation == 0 && value == 'summer vacation') {
                return "You don't have enought summer school left";
              }
              return null;
            },
            onSaved: (String value) {
              _requestLeaveFormData['type'] = value;
            },
          )
        ]
      )
    );
  }



  /******************************/
  /********** METHODS ***********/ 
  /******************************/


  /*
  * SUBMIT FORM
  */
  void _submitForm(BuildContext context, Function addRequestLeave, Function updateRequestLeave, Function selectRequest) async {
  // if input empty or not correct => show error message and stop function
  if(!_requestLeaveFormKey.currentState.validate()) {
    return;
  }

  // saveData in variables
  _requestLeaveFormKey.currentState.save();

    //http request
    bool response;
    print(_requestLeaveFormData['startDate']);
    if(widget.request == null) {
      response = await addRequestLeave(
        DateFormat('yyyy/MM/dd').format(_requestLeaveFormData['startDate']),
        DateFormat('yyyy/MM/dd').format(_requestLeaveFormData['endDate']),
        double.parse(_requestLeaveFormData['nbDays']),
        _requestLeaveFormData['type'],
        _requestLeaveFormData['image']
      );
    } else {
      selectRequest(widget.request.id);
      response = await updateRequestLeave(
        DateFormat('yyyy/MM/dd').format(_requestLeaveFormData['startDate']),
        DateFormat('yyyy/MM/dd').format(_requestLeaveFormData['endDate']),
        double.parse(_requestLeaveFormData['nbDays']),
        _requestLeaveFormData['type'],
        _requestLeaveFormData['image']
      );
    }   
    
    //handle response
    if (response) {
      print(response);
      Navigator.pushReplacementNamed(context, '/');
    } 
  }



  void _setImage(File image) {
    _requestLeaveFormData['image'] = image;
  }

}