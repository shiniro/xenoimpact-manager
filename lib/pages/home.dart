import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../widgets/helpers/global_translation.dart';

import '../scoped-models/main.dart';
import './leaves/request_leave.dart';
import './leaves/request_status.dart';
import '../widgets/layout/global_layout.dart';
import '../widgets/ui_elements/colored_tab_bar.dart';
import '../widgets/ui_elements/contact_fab.dart';

class HomePage extends StatefulWidget {
  final MainModel model;

  HomePage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
      initialIndex: 0,
    )..addListener(() {
      setState(() {});
    });
    super.initState();
  }


  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("request_list.request_list"),
      showAppHeader: true,
      actions: <Widget>[
        _buildAppHeaderActions()
      ],
      body: Container(
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0),
              child: ColoredTabBar(
                color: Colors.white,
                tabBar:TabBar(
                  controller: _tabController,
                  indicatorWeight: 3.0,
                  labelColor: Theme.of(context).accentColor,
                  indicatorColor: Theme.of(context).accentColor,
                  unselectedLabelColor: Color(0xffcccccc),
                  tabs: <Widget>[
                    Tab(
                      text: allTranslations.text("request_list.request_status"),
                    ),
                    Tab(
                      text: allTranslations.text("request_list.request_leave"),
                    )
                  ],
                ),
              ),
            ),
            body: TabBarView(
              controller: _tabController,
              children: <Widget>[
                RequestStatusPage(widget.model, false),
                RequestLeavePage(widget.model),
              ]
            ),
            floatingActionButton: _tabController.index == 0
            ? ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
                return  model.isLoading ? Container() : ContactFab(widget.model.authenticatedUser);
              })
            : Container(),
          ),
        )
      ) 
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * APP HEADER ACTIONS
  * build icon (filter)
  */
  Widget _buildAppHeaderActions() {
    return IconButton(
      icon: Image.asset(
        'assets/img/icon_filter.png',
        width: 22,
        height: 21,
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/filters/home');
      },
    );
  }
}