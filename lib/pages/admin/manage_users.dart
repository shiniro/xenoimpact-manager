import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../models/user.dart';

import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/tile_text.dart';
import '../../widgets/ui_elements/user_image.dart';
import '../../widgets/ui_elements/switch_list_user.dart';

class UsersPage extends StatefulWidget {
  final MainModel model;

  UsersPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _UserPageState();
  }
}

class _UserPageState extends State<UsersPage> {

  @override
  void initState() {
    widget.model.fetchUsers(widget.model.authenticatedUser.company.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text('side_menu.manage_users'),
      showAppHeader: true,
      body: ScopedModelDescendant<MainModel>(builder: (BuildContext context, Widget child, MainModel model) {
        Widget content = Center(child: Text(allTranslations.text('user_list.no_user_found')));
        if(model.isLoading) {
          content = Center(child: CircularProgressIndicator());
        } else {
          content = Container(
            child: Column(children: <Widget>[
              SizedBox(height: 20.0),
              _buildExplainationText(),
              SizedBox(height: 20.0),
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: model.allUsers.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildTiles(model.allUsers[index], model);
                  },
                )
              )
            ],)
          );
        }
        return RefreshIndicator(
          child: content, 
          onRefresh: () => model.fetchRequests(),
        );
      },)
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/


  /*
  * Explaination Text
  */
  Widget _buildExplainationText() {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    return Container(
      width: targetWidth,
      padding: EdgeInsets.all(20.0),
      child: Text(allTranslations.text('manage_user.admin_explaination')),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: Color(0xffedf1f2),
      )
    );
  }



  /*
  * BUILD LIST OF TILES
  */
  Widget _buildTiles(User user, MainModel model) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Color(0xffedf1f2))
        ),
      ),
      child: ListTile(
        contentPadding: EdgeInsets.all(0.0),
        onTap: () {
          Navigator.pushNamed<dynamic>(context, '/admin/users/' + user.id);
        },
        leading: Container(
          padding: EdgeInsets.only(left: 20.0),
          width: 70.0,
          height: 50.0,
          child: user.pictureUrl != null 
            ? UserImage(imagePath: user.pictureUrl)
            : UserImage(imagePath: 'assets/img/user-no-picture.jpg', internalImage: true),
        ),
        title: TileText(label: allTranslations.text('name'), value: user.name),
        subtitle: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 5.0),
              TileText(label: allTranslations.text('email'), value: user.email)
            ],
          )
        ),
        trailing: Container(
          width: 100.0,
          padding: EdgeInsets.all(0.0),
          margin: EdgeInsets.all(0.0),
          child: SwitchListUser(model: model, id: user.id, initialValue: user.admin, modifyState: model.updateSelectedUserAdmin)
        ),
      )
    );
  }
}