import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/full_width_button.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

class ManageRequestStatus extends StatefulWidget {
  final MainModel model;

  ManageRequestStatus(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ManageRequestStatusState();
  }
}

class _ManageRequestStatusState extends State<ManageRequestStatus> {

  final Map<String, dynamic> _manageRequestFormData = {
    'status': null,
    'comment': null
  };

  final GlobalKey<FormState> _manageRequestFormKey = GlobalKey<FormState>();
  final List<String> _leaveStatus = ['pending', 'approved', 'rejected'];
  String _currentStatusSelected;

  @override
  void initState() {
    setState(() {
      _currentStatusSelected = widget.model.selectedRequestStatus;
    });
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("request_form.request_status_change"),
      showAppHeader: true,
      appBarLeading: IconButton(icon:Icon(Icons.arrow_back),
        onPressed:() => Navigator.pop(context, false),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _manageRequestFormKey,
          child: Column(children: <Widget>[
            _buildDropdownStatus(context),
            SizedBox(height: 20.0),
            _buildCommentTextField(context),
            SizedBox(height: 20.0),
            FullWidthButton(
              text: allTranslations.text("request_form.modify_status"),
              onPressed: () => _submitForm(context, widget.model.updateRequestStatus),
            )
          ],)
        )
      )
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/


  /*
  * STATUS DROPDOWN
  */
  Widget _buildDropdownStatus(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text("status")),
          SizedBox(height: 10.0),
          DropdownButtonFormField<String>(
            decoration: buildInputDecoration(
              context: context, 
              placeholder: '',
              icon: Icons.local_offer
            ),
            items: _leaveStatus.map((String status){
              return DropdownMenuItem<String>(
                value: status,
                child: Text(allTranslations.text(status.replaceAll(new RegExp(r' '), '_'))),
              );
            }).toList(),
            value: _currentStatusSelected,
            onChanged: (String value) {
              setState((){
                _currentStatusSelected = value;
              });
            },
            onSaved: (String value) {
              _manageRequestFormData['status'] = value;
            },
          )
        ],
      )
    );
  }



  /*
  * COMMENT TEXT FIELD
  */
  Widget _buildCommentTextField(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text("comment")),
          SizedBox(height: 10.0),
          TextFormField(
            maxLines: null,
            keyboardType: TextInputType.multiline,
            initialValue: widget.model.selectedRequestComment != null ? widget.model.selectedRequestComment : '',
            decoration: buildInputDecoration(
              context: context,
              placeholder: allTranslations.text("request_form.comment_here")
            ),
            onSaved: (String value) {
              _manageRequestFormData['comment'] = value;
            },
          )
        ],
      )
    );
  }



  /******************************/
  /********** METHODS ***********/ 
  /******************************/

  /*
  * SUBMIT FORM
  */
  void _submitForm(BuildContext context, Function updateRequestStatus) async {
  // if input empty or not correct => show error message and stop function
  if(!_manageRequestFormKey.currentState.validate()) {
    return;
  }

  // saveData in variables
  _manageRequestFormKey.currentState.save();

    //http request
    bool response = await updateRequestStatus(
      _manageRequestFormData['status'],
      _manageRequestFormData['comment']
    );
    
    //handle response
    if (response != null) {
      print(response);
      Navigator.pop(context);
    } 
  }
}