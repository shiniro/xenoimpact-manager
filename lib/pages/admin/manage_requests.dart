import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../scoped-models/main.dart';
import './../leaves/request_status.dart';
import '../../widgets/layout/global_layout.dart';

class ManageRequestsPage extends StatefulWidget {
  final MainModel model;

  ManageRequestsPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ManageRequestsPageState();
  }
}

class _ManageRequestsPageState extends State<ManageRequestsPage> {
  
  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text("request_list.request_management"),
      showAppHeader: true,
      actions: <Widget>[
        _buildAppHeaderActions()
      ],
      body: RequestStatusPage(widget.model, true),
    );
  }


  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * APP HEADER ACTIONS
  * build icon (filter)
  */
  Widget _buildAppHeaderActions() {
    return IconButton(
      icon: Image.asset(
        'assets/img/icon_filter.png',
        width: 22,
        height: 21,
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/filters/admin');
      },
    );
  }
}