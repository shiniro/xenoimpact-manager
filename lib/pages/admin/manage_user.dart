import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/buttons_number_input.dart';
import '../../widgets/ui_elements/common_switch.dart';
import '../../widgets/ui_elements/block_text.dart';
import '../../widgets/ui_elements/title_block.dart';
import '../../scoped-models/main.dart';

class ManageUserPage extends StatefulWidget {
  final MainModel model;

  ManageUserPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ManageUserPageState();
  }
}

class _ManageUserPageState extends State<ManageUserPage> {

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: '${allTranslations.text('manage_user.manage_user')} ${widget.model.selectedUser.name}',
      showAppHeader: true,
      appBarLeading: IconButton(icon:Icon(Icons.arrow_back),
        onPressed:() => Navigator.pop(context, false),
      ),
      body: ListView(
        children: <Widget>[
          TitleBlock(title: allTranslations.text("manage_user.manage_days_off")),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Column(children: <Widget>[
              _buildLeavesCountBlock(widget.model),
              SizedBox(height: 10.0),
              ButtonsNumberInput(
                label: allTranslations.text('request_list.days_off'),
                initialValue: widget.model.selectedUser.daysOff,
                modifyState: widget.model.updateSelectedUserDaysOff
              ),
              ButtonsNumberInput(
                label: allTranslations.text('request_list.summer_vacation'),
                initialValue: widget.model.selectedUser.summerVacation,
                modifyState: widget.model.updateSelectedUserSummerVacation
              ),
            ],),
          ),
          TitleBlock(title: allTranslations.text("manage_user.manage_user_settings")),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Column(children: <Widget>[
              widget.model.selectedUser.vacationRule == null ? ButtonsNumberInput(
                label: allTranslations.text('manage_user.number_days_off_per_year'),
                initialValue: widget.model.selectedUser.nbDaysOffPerYear != null ? widget.model.selectedUser.nbDaysOffPerYear : 15.0,
                modifyState: widget.model.updateSelectedUserNbDaysOffPerYear
              ) : Container(),
              widget.model.selectedUser.vacationRule == null ? ButtonsNumberInput(
                label: allTranslations.text('manage_user.number_summer_vacation_per_year'),
                initialValue: widget.model.selectedUser.nbSummerVacationPerYear != null ? widget.model.selectedUser.nbSummerVacationPerYear : 4.0,
                modifyState: widget.model.updateSelectedUserNbSummerVacationPerYear
              ) : Container(),
              _buildSwitchExplaination(),
              _buildSwichWithLabel(
                allTranslations.text('manage_user.autoActionOnChangeYear'),
                widget.model.selectedUser.autoActionOnChangeYear,
                widget.model.updateSelectedUserAutoActionOnChangeYear
              ),
              _buildSwichWithLabel(
                allTranslations.text('manage_user.resetOnChangeYear'),
                widget.model.selectedUser.resetOnChangeYear,
                widget.model.updateSelectedUserResetOnChangeYear
              ),
            ]),
          )
        ],
      )
    );
  }



  /*
  * LEAVES COUNTS BLOCK
  */
  Widget _buildLeavesCountBlock(MainModel model) {
    return Container(
      child: BlockText(
        child: RichText(
          text: TextSpan(
            style: TextStyle(color: Theme.of(context).textTheme.body1.color),
            children: <TextSpan>[
              TextSpan(
                text: model.selectedUser.name,
                style: TextStyle(color: Theme.of(context).accentColor)
              ),
              TextSpan(
                text: ' ${allTranslations.text('manage_user.took')} ',
              ),
              TextSpan(
                text: model.selectedUser.daysOffTaken.toString(),
                style: TextStyle(color: Theme.of(context).accentColor)
              ),
              TextSpan(
                text: ' ${allTranslations.text('manage_user.days_off_and')} '
              ),
              TextSpan(
                text: model.selectedUser.daysOffNoPaidTaken.toString(),
                style: TextStyle(color: Theme.of(context).accentColor)
              ),
              TextSpan(
                text: ' ${allTranslations.text('manage_user.days_off_no_paid_and')} '
              ),
              TextSpan(
                text: model.selectedUser.summerVacationTaken.toString(),
                style: TextStyle(color: Theme.of(context).accentColor)
              ),
              TextSpan(
                text: ' ${allTranslations.text('manage_user.summer_vacation_days')}.'
              ),
            ]
          )
        )
      )
    );
  }



  /*
  * BUILD SWITCH EXPLAINATION
  */
  Widget _buildSwitchExplaination() {
    return BlockText(
      child: Container(
        child: Column(children: <Widget>[
          RichText(
            text: TextSpan(
              style: TextStyle(color: Theme.of(context).textTheme.body1.color),
              children: <TextSpan>[
                TextSpan(
                  text: allTranslations.text('manage_user.autoActionOnChangeYear'),
                  style: TextStyle(color: Theme.of(context).accentColor)
                ),
                TextSpan(
                  text: ': ${allTranslations.text('manage_user.auto_explanation')} ',
                ),
              ]
            ),
          ),
          SizedBox(height: 10.0),
          RichText(
            text: TextSpan(
              style: TextStyle(color: Theme.of(context).textTheme.body1.color),
              children: <TextSpan>[
                TextSpan(
                  text: allTranslations.text('manage_user.resetOnChangeYear'),
                  style: TextStyle(color: Theme.of(context).accentColor)
                ),
                TextSpan(
                  text: ': ${allTranslations.text('manage_user.reset_explanation')} ',
                ),
              ]
            ),
          ),
        ],)
      )
    );
  }



  /*
  * build Switch With Label
  */
  Widget _buildSwichWithLabel(String label, bool value, Function modifyState) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(label),
          Container(
            child: CommonSwitch(initialValue: value, modifyState: modifyState),
          )
        ]
      )
    );
  }
}