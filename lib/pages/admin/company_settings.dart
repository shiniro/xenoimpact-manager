import 'package:flutter/material.dart';
import '../../widgets/helpers/global_translation.dart';

import '../../widgets/layout/global_layout.dart';
import '../../widgets/ui_elements/common_input_decoration.dart';

import '../../widgets/ui_elements/location_input.dart';

import '../../scoped-models/main.dart';

class CompanySettings extends StatefulWidget {
  final MainModel model;

  CompanySettings(
    this.model
  );

  @override
  State<StatefulWidget> createState() {
    return CompanySettingsState();
  }
}

class CompanySettingsState extends State<CompanySettings> {

  final GlobalKey<FormState> _companySettingsFormKey = GlobalKey<FormState>();

  final List<String> _supportedLanguages = ['kr', 'en','fr'];
  String _currentLanguageSelected = allTranslations.currentLanguage;

  final Map<String, dynamic> _companyFormData = {
    'name': null,
    'email': null,
    'emailBase': null,
    'phone': null,
    'language': null,
    'address': null,
    'country': null,
    'vacationRules': null,
    'defaultRule': null
  };

  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: allTranslations.text('side_menu.manage_users'),
      showAppHeader: true,
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Form(
            key: _companySettingsFormKey,
            child: Column(
              children: <Widget>[
                _buildNameTextField(),
                SizedBox(height: 20.0),
                _buildEmailTextField(),
                SizedBox(height: 20.0),
                _buildEmailBaseTextField(),
                SizedBox(height: 20.0),
                _buildPhoneTextField(),
                SizedBox(height: 20.0),
                _buildDropdownLanguage(),
                SizedBox(height: 20.0),
                LocationInput()
              ]
            )
          )
        ]
      )
    );
  }



  /*
  * BUILD NAME TEXT FIELD
  */
  Widget _buildNameTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('name')),
          SizedBox(height: 10.0),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: allTranslations.text('name'),
              icon: Icons.person
            ),
            // initialValue: widget.model.authenticatedUser. != null && widget.request.nbDays != null ? widget.request.nbDays.toString() : '',
            validator: (String value) {
              if (value.isEmpty || value.length < 3) {
                return 'your name is not long enough';
              }
              return null;
            },
            onSaved: (String value) {
              _companyFormData['name'] = value;
            },
          )
        ],
      )
    );
  }


  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('email')),
          SizedBox(height: 10.0),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: allTranslations.text("authentification.email_placeholder"),
              icon: Icons.email
            ),
            // initialValue: widget.model.authenticatedUser. != null && widget.request.nbDays != null ? widget.request.nbDays.toString() : '',
            validator: (String value) {
              if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
                return '가입하신 이메일에서 임시 비밀번호를 확인하세요';
              }
              return null;
            },
            onSaved: (String value) {
              _companyFormData['email'] = value;
            },
          )
        ],
      )
    );
  }



  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailBaseTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('manage_company.email_base')),
          SizedBox(height: 10.0),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: allTranslations.text("manage_company.placeholder_email_base"),
              icon: Icons.email
            ),
            // initialValue: widget.model.authenticatedUser. != null && widget.request.nbDays != null ? widget.request.nbDays.toString() : '',
            validator: (String value) {
              if (value.isEmpty || !RegExp(r"^@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
                return '가입하신 이메일에서 임시 비밀번호를 확인하세요';
              }
              return null;
            },
            onSaved: (String value) {
              _companyFormData['email'] = value;
            },
          )
        ],
      )
    );
  }




  /*
  * LANGUAGE DROPDOWN
  */
  Widget _buildDropdownLanguage() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('settings.languages')),
          SizedBox(height: 10.0),
          DropdownButtonFormField<String>(
            decoration: buildInputDecoration(
              context: context, 
              placeholder: '',
              icon: Icons.local_offer
            ),
            items: _supportedLanguages.map((String type){
              return DropdownMenuItem<String>(
                value: type,
                child: Text(allTranslations.text('settings.${type.replaceAll(new RegExp(r' '), '_')}')),
              );
            }).toList(),
            value: _currentLanguageSelected,
            onChanged: (String value) {
              setState((){
                _currentLanguageSelected = value;
              });
            },
            onSaved: (String value) {
              _companyFormData['language'] = value;
            },
          )
        ]
      )
    );
  }



  /*
  * BUILD PHONE TEXT FIELD
  */
  Widget _buildPhoneTextField() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(allTranslations.text('manage_company.phone')),
          SizedBox(height: 10.0),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: buildInputDecoration(
              context: context, 
              placeholder: allTranslations.text("manage_company.phone"),
              icon: Icons.email
            ),
            // initialValue: widget.model.authenticatedUser. != null && widget.request.nbDays != null ? widget.request.nbDays.toString() : '',
            validator: (String value) {
              if (value.isEmpty || !RegExp(r"^(?:(?:\+|00)82|0)(\s*[1-9]|\s*[0-9][0-9])(?:[\s.-]*\d{4}|[\s.-]*\d{3})(?:[\s.-]*\d{4})$").hasMatch(value)) {
                return '가입하신 이메일에서 임시 비밀번호를 확인하세요';
              }
              return null;
            },
            onSaved: (String value) {
              _companyFormData['phone'] = value;
            },
          )
        ],
      )
    );
  }
}