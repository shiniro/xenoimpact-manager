const functions = require('firebase-functions');
const cors = require('cors')({origin: true});
const Busboy = require('busboy');
const os = require('os');
const path = require('path');
const fs = require('fs');
const fbAdmin = require('firebase-admin');
const uuid = require('uuid/v4');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const projectId = 'xenoimpact-manager';
const keyFilename = 'xenoimpact-manager-firebase-adminsdk-4rzoe-a3bf65292e.json';

const {
    Storage
} = require('@google-cloud/storage');

const gcs = new Storage({
    projectId: projectId,
    keyFilename: keyFilename
});

fbAdmin.initializeApp({credential: fbAdmin.credential.cert(require('./xenoimpact-manager-firebase-adminsdk-4rzoe-a3bf65292e.json'))})

exports.storeImage = functions.https.onRequest((request, response) => {
  return cors(request, response, () => {
    if(request.method !== 'POST') {
      return response.status(500).json({ message: 'Not allowed.' });
    }

    if(!request.headers.authorization || !request.headers.authorization.startsWith('Bearer ')) {
      return response.status(401).json({ error: 'Unauthorized.' })
    }

    let idToken;
    idToken = request.headers.authorization.split('Bearer ')[1];

    const busboy = new Busboy({headers: request.headers});
    let uploadData;
    let oldImagePath;

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      const filePath = path.join(os.tmpdir(), filename);
      uploadData = {filePath: filePath, type: mimetype, name: filename};
      file.pipe(fs.createWriteStream(filePath));
    });

    busboy.on('field', (filename, value) => {
      oldImagePath = decodeURIComponent(value);
    });

    busboy.on('finish', () => {
      const bucket = gcs.bucket('xenoimpact-manager.appspot.com');
      const id = uuid();
      let imagePath = 'images/' + id + '-' + uploadData.name;

      if(oldImagePath) {
        imagePath = oldImagePath;
      }

      return fbAdmin.auth()
        .verifyIdToken(idToken)
        .then(decodedToken => {
          return bucket.upload(uploadData.filePath, {
            uploadType: 'media',
            destination: imagePath,
            metadata: {
              metadata: {
                contentType: uploadData.type,
                firebaseStorageDownloadTokens: id
              }
            }
          })
        }).then(() => {
          return response.status(201).json({
            imageUrl: 'https://firebasestorage.googleapis.com/v0/b/' + bucket.name + '/o/' + encodeURIComponent(imagePath) + '?alt=media&token=' + id,
            imagePath: imagePath
          })
        }).catch(error => {
          return response.status(401).json({error: 'Unauthorized !'});
        });

    });
    return busboy.end(request.rawBody);
  });
});



exports.deleteImage = functions.database.ref('/requests').onDelete(
  (snapshot) => {
    const imageData = snapshot.val();
    const imagePath = imageData.imagePath;

    const bucket = gcs.bucket('xenoimpact-manager.appspot.com');
    return bucket.file(imagePath).delete();
  }
);


exports.listAllUsers = functions.https.onRequest((req, res) => {
  // List batch of users, 1000 at a time.
  var allUsers = [];

  return fbAdmin.auth().listUsers()
    .then(function (listUsersResult) {
      listUsersResult.users.forEach(function (userRecord) {
        // For each user
        var userData = userRecord.toJSON();
        allUsers.push(userData);
      });
      res.status(200).send(JSON.stringify(allUsers));
    })
    .catch(function (error) {
      console.log("Error listing users:", error);
      res.status(500).send(error);
    });
});


exports.listAllUsers = functions.https.onCall((data, context) => {
  // List all users
  return listAllUsers();
});